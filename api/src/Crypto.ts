import argon2 from "argon2";
import crypto from "crypto";

type passwordModifier = (password: string) => Promise<string>;

const hashLen = 64;

const genRandomString = (length: number): string => {
  return crypto.randomBytes(Math.ceil(length / 2))
    .toString("hex")    /** convert to hexadecimal format */
    .slice(0, length);   /** return required number of characters */
};

const hashAndSalt: passwordModifier = async (password) => {
  const salt = genRandomString(hashLen);

  const hash =
  await argon2.hash(password + salt, { type: argon2.argon2id });

  return salt + hash;
};

const verifyPass = async (
  password: string,
  hash: string): Promise<boolean> => {

  return await argon2.verify(
    hash.slice(hashLen),
    password + hash.slice(0, hashLen)) ? true : false ;
};

export { hashAndSalt, verifyPass };
