import { Client } from "cassandra-driver";
import fs from "fs";
import jwt from "jsonwebtoken";
import path from "path";
import { promisify } from "util";

const cert: Buffer =
fs.readFileSync(path.join(__dirname, "../.keys/private.key"));

interface Payload {
  data: string;
}

const makeJWT = (data: string): string => {
    return jwt.sign({ data }, cert, { algorithm: "HS512"});
};

const isPayload = (arg: any): arg is Payload => {
  if ("data" in arg && typeof arg.data === "string") {
    return true;
  } else {
    return false;
  }
};

const jwtVerify = promisify(jwt.verify);

const verifyJWT = async (
  token: string,
  client: Client): Promise<Payload> => {
  const query = `
  SELECT COUNT(*)
  FROM ourlines.invalid_tokens
  WHERE jwt='${token}' ;
  `;

  const result = await client.execute(query);

  if (result.rows[0].count.toJSON() === "1") {
    throw new Error("Invalid token");
  }

  const decodedToken = await jwtVerify(token, cert);

  if (isPayload(decodedToken)) {
    return decodedToken;
  } else {
    throw new Error("No data");
  }
};

export { makeJWT, verifyJWT };
