import { types } from "cassandra-driver";
import { Context } from "ourlineslib";
import { verifyJWT } from "../JWT";
import { eventsWithMember, SolrEvent, solrToEvent, notificationsForUser, solrNotifToNotification } from "../Solr";

export const Query = {
    currentUser: async (
        _: void,
        args: any,
        context: Context): Promise<types.Row> => {
       const { client } = context;

       const { jwt } = args.input;

       const { data } = await verifyJWT(jwt, client);

       const query = `
       SELECT *
       FROM ourlines.users
       WHERE id=?`;

       const result =
       await client.execute(query, [ data ], { prepare: true });

       return result.first();
   },
    event: async (
        _: void,
        args: any,
        context: Context
    ): Promise<any> => {
        const { client } = context;

        const query = `
        SELECT *
        FROM ourlines.events
        WHERE id=?`;

        const resp = await client.execute(
            query,
            [ args.input.id ],
            { prepare: true });

        return resp.first();
   },
    events: async (
       _: void,
       args: any,
       context: Context):
       Promise<any> => {

        const { client } = context;

        const jwt = await verifyJWT(args.input.jwt, client);

        if (!jwt) {
            throw new Error("Authentication failed");
        }

        const resp = await eventsWithMember(jwt.data);

        const events = resp.response.docs.map(
            (x: SolrEvent) => solrToEvent(x));

        return { events };
    },
    notifications: async (
        _: void,
        args: any,
        context: Context
    ) => {
        const { client } = context;

        const { data } = await verifyJWT(args.input.jwt, client);

        const res = await notificationsForUser(data);

        return res.response.docs.map(solrNotifToNotification)
    },
    picture: async (
        _: void,
        args: any,
        context: Context): Promise<types.ResultSet> => {

       const { client } = context;

       const query: string = "SELECT * FROM pictures WHERE id=?";

       return client.execute(
           query, [ args.id ], { prepare: true }
       );
    },
    users: async (
        _: void,
        args: any,
        context: Context): Promise<types.Row[]> => {
        const { client } = context;

        const { ids } = args.input;

        const queries: string[] =
        ids.map(
            (id: string) => {
                return `SELECT * FROM ourlines.users WHERE id=${id}`;
            }
        );

        const resultSet = await Promise.all(
            queries.map(
                (query) => client.execute(
                    query, undefined, { prepare: true })
            )
        );

        const results: types.Row[] = [];

        resultSet.forEach((val) => {
            results.push(val.first());
        });

        return results;
   }
};
