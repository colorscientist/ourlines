import { PutObjectRequest } from "aws-sdk/clients/s3";
import { types } from "cassandra-driver";
import fileType from "file-type";
import shortid from "shortid";
import uuid from "uuid/v4";

import {
    Availability,
    Context,
    Location,
    User
} from "ourlineslib";
import { hashAndSalt, verifyPass } from "../Crypto";
import { makeJWT, verifyJWT } from "../JWT";
import { eventToSolr, uploadEvent, uploadNotification, SolrNotif, removeSolrNotif } from "../Solr";
import { addEmailToBlast } from "../Solr";

interface Session {
    jwt: string;
    user: User;
}

export interface CreateEventInput {
    id?: string;
    jwt?: string;
    name: string;
    ownerId: string;
    members: string[];
    ispublic: boolean;
    location?: Location;
}

export const Mutation = {
    addEmailToBlast: async (
        _: void,
        args: any,
        context: Context
    ) => {
        const { email } = args.input;

        const { client } = context;

        const query = `
        SELECT *
        FROM ourlines.blasts_by_name
        WHERE name = '${args.input.blastname}' ;`

        const res = await client.execute(query)

        if (res.first()['id']) {
            addEmailToBlast(res.first()['id'], email);
        } else {
            throw new Error("No such email blast")
        }
    },
    changeProfilePicture: async (
        _: void,
        args: any,
        context: Context
    ): Promise<types.ResultSet> => {
        const { client, s3, logger } = context;

        const {
            stream,
            mimetype,
            encoding
        } = await args.input.upload;
        const { userId } = await args.input.user;

        const imageFileType = fileType(stream);

        const s3Params: PutObjectRequest = {
            Body: stream,
            Bucket: "app.ourlines.media",
            Key: shortid() + imageFileType.ext
        };

        try {
            await s3.putObject(s3Params).promise();
        } catch (e) {
            logger.error(e);
        }

        const query = `
        UPDATE ourlines.users
        SET picture = :picture
        WHERE id = :userId ;
        `;

        const params = {
            picture: {
                bucket: s3Params.Bucket,
                encoding,
                key: s3Params.Key,
                mimetype
            },
            userId
        };

        return client.execute(query, params, { prepare: true });
    },
    confirmFriendRequest: async (
        _: void,
        args: any,
        context: Context): Promise<types.ResultSet> => {
        const { client } = context;

        const { data } = await verifyJWT(args.input.jwt, client);

        const userQuery: string = `UPDATE ourlines.users
        SET friends = friends + { ${args.input.friendid } },
        friendRequests = friendRequests - { ${args.input.friendid } }
        WHERE id = ${ data } ;
        `;

        const friendQuery: string = `UPDATE ourlines.users
        SET friends = friends + { ${ data } }
        WHERE id = ${ args.input.friendid } ;
        `;

        const removeNotificationQuery = `DELETE FROM
        ourlines.notifications
        WHERE id=${args.input.requestid}
        `;

        removeSolrNotif(args.input.requestid);

        return client.batch([userQuery, friendQuery, removeNotificationQuery], { prepare: true });
    },
    createEvent: async (
        _: void,
        args: any,
        context: Context) => {
        const { client } = context;

        const {
            jwt,
            ...inputs
        }: CreateEventInput = args.input;

        const {
            name,
            ownerId,
            members,
            ispublic
        } = inputs;

        const id = uuid();

        const verified = jwt ?
        await verifyJWT(jwt, client) : { data: undefined };

        if (verified.data !== ownerId) {
            throw new Error("Token/owner ID mismatch");
        }

        const query = `INSERT INTO ourlines.events
        (id, name, owner, members, ispublic)
        VALUES
        (${id}, ?, ${ownerId}, {${members}}, ${ispublic} ) ;
        `;

        uploadEvent([eventToSolr({ id, ...inputs })]);

        return client.execute(query, [ name ]);
    },
    deleteAccount: async (
        _: void,
        args: any,
        context: Context
    ) => {
        const { client } = context;

        const { jwt } = args.input;

        const { data } = await verifyJWT(jwt, client);

        const query = `
        DELETE FROM ourlines.users
        WHERE id = ? ;
        `;

        const result = await client.execute(query, [ data ], { prepare: true });

        return result.wasApplied();
    },
    joinEvent: async (
        _: void,
        args: any,
        context: Context
    ) => {
        const { client } = context;

        const { id, jwt } = args.input;

        const { data } = await verifyJWT(jwt, client);

        const query = `
        UPDATE ourlines.events
        SET members = members + { ${ data } }
        WHERE id = ${ id };`;

        const resp = await client.execute(
            query,
            [],
            { prepare: true }
        );

        return resp.first();
    },
    logIn: async (
        _: void,
        args: any,
        context: Context): Promise<Session> => {
        const { password, email } = args.input;

        const { client } = context;

        const query: string = `
        SELECT
        id, name, availability,
        picture, interests, friends, muted, password
        FROM ourlines.users_by_email WHERE email=?;
        `;

        const result = await client.execute(
            query,
            [ email ],
            { prepare: true});

        if (!result) {
            throw new Error("Email not found");
        }

        const passSuccess = await verifyPass(
            password,
            result.rows[0].password);

        if (!passSuccess) {
            throw new Error("Password is incorrect");
        }

        const user: User = {
            availability: result.rows[0].availability,
            email,
            friends: result.rows[0].friends,
            id: result.rows[0].id,
            interests: result.rows[0].interests,
            muted: result.rows[0].muted,
            name: result.rows[0].name,
            picture: result.rows[0].picture
        };

        const jwt = makeJWT(user.id);

        return { user, jwt };
    },
    muteUser: async (
        _: void,
        args: any,
        context: Context): Promise<types.ResultSet> => {
        const { client } = context;

        const query: string = `UPDATE ourlines.users
        SET muted = muted + { ${args.input.mutedid } }
        WHERE id = ${ args.input.userid } ;`;

        return client.execute(query, { prepared: true });
    },
    sendFriendRequest: async (
        _: void,
        args: any,
        context: Context): Promise<types.ResultSet[]> => {
        const { client } = context;

        const { data } = await verifyJWT(args.input.jwt, client);

        const friendInfoReq = `
        SELECT id
        FROM ourlines.users_by_email
        WHERE email = '${ args.input.friendemail }' ;
        `;

        const friendInfo = await client.execute(friendInfoReq);

        const query: string = `UPDATE ourlines.users
        SET friendRequests = friendRequests + { ${ data } }
        WHERE id = ${ friendInfo.first()["id"] } ;
        `;

        const id = uuid();

        const setNotifQuery = `
        INSERT INTO ourlines.notifications
        (id, category, userid, itemid)
        VALUES
        (${id}, ${0}, ${friendInfo.first()["id"]}, ${data})
        `;

        const notif: SolrNotif = {
            id,
            userid_s: friendInfo.first()["id"],
            itemid_s: data,
            category_i: 0
        };

        uploadNotification([notif]);

        return Promise.all([client.execute(query), client.execute(setNotifQuery)])
    },
    setAvailability: async (
        _: void,
        args: any,
        context: Context
    ) => {
        const { client } = context;

        const payload = await verifyJWT(args.input.jwt, client);

        if (!Object.values(Availability).includes(args.input.availability)) { 
            throw new Error("No such availability status");
        }

        const query: string = `UPDATE ourlines.users
        SET availability = '${Availability[args.input.availability]}'
        WHERE id = ${payload.data} ;
        `;

        return client.execute(query, [], { prepare: true });
    },
    signOut: async (
        _: void,
        args: any,
        context: Context) => {
        const { client } = context;

        const { jwt } = args.input;

        const query = `INSERT INTO ourlines.invalid_tokens
        ( jwt )
        VALUES ('${jwt}')
        `;

        const result = await client.execute(query);

        return result.wasApplied();
    },
    signUp: async (
        _: void,
        args: any,
        context: Context): Promise<Session> => {
        const { client } = context;

        const { email, password } = args.input;

        const checkExistingAccQuery = `
        SELECT COUNT(*)
        FROM ourlines.users_by_email
        WHERE email = '${email}' ;
        `;

        const checkExistingAcc = await client.execute(checkExistingAccQuery);

        if (checkExistingAcc.rows[0]["count"] > 0 ) {
            throw new Error("An account with this email already exists");
        }

        const hashedPass = await hashAndSalt(password);

        const user = {
            availability: Availability.AVAILABLE,
            email,
            friends: [],
            id: uuid(),
            interests: [],
            muted: [],
            name: "",
            password: hashedPass,
            picture: undefined
        };

        const query = `
        INSERT INTO ourlines.users
        (id, name, email, availability, password)
        VALUES (:id, :name, :email, :availability, :password)
        `;

        const result = await client.execute(
            query,
            user,
            { prepare: true });

        const jwt = makeJWT(user.id);

        if (result.wasApplied()) {
            return { jwt, user };
        } else {
            throw new Error("Signup failure, please try again");
        }
    }
};
