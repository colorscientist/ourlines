import fetch, { RequestInit } from "node-fetch";
import { CreateEventInput } from "./resolvers/Mutation";
import uuid from "uuid/v4";

const baseUrl = "http://localhost:8983/api/cores";

const collections = {
    email_blasts: "/blasts/",
    events: "/events/",
    notifications: "/notifications/"
};

const readStream = async (
    stream: NodeJS.ReadableStream): Promise<any> => {
    let content = "";

    stream.on("data", (chunk) => {
        content += chunk.toString();
    });

    return new Promise((resolve, reject) => {
        stream.on("end", () => {
            resolve(JSON.parse(content));
        });

        stream.on("error", (e) => {
            reject(e);
        });
    });
};

export const isSolrConnected = async (): Promise<boolean> => {
    const res = await fetch(baseUrl);

    return res.ok;
};

interface DistAndCoords {
    dist: number;
    coords: {
        lat: number;
        lon: number
    };
}

export const addEmailToBlast = async (blast_id: string, email: string) => {
    const email_entry = {
        id: uuid(),
        blastid_s: blast_id,
        email_s: email
    }

    const init: RequestInit = {
        body: JSON.stringify(email_entry),
        headers: { "Content-Type": "application/json" },
        method: "post"
    }

    const url = baseUrl + collections.email_blasts + `update?commit=true`;

    fetch(url, init);
};

export const getEmailsForBlast = async (blast_id: string) => {
    const query  = `query?q=blastid_s:(${blast_id})&fl=email_s`;

    const url = baseUrl + collections.email_blasts + query;

    return fetch(url)
};

export interface SolrEvent {
    id: string;
    name_t: string;
    loc_p?: string;
    members_ss?: string[];
}

export interface SolrNotif {
    id: string;
    userid_s: string;
    itemid_s: string;
    category_i: number;
}

export const eventToSolr = (event: CreateEventInput): SolrEvent => {
    const coordinates = event.location && event.location.coordinates ? `
    ${event.location.coordinates.lat},${event.location.coordinates.lon}
    ` : undefined;

    if (!event.id) {
        throw new Error("No event ID");
    }

    const solrEvent: SolrEvent = {
        id: event.id,
        loc_p: coordinates,
        members_ss: event.members,
        name_t: event.name
    };

    return solrEvent;
};

export const solrToEvent = (solr: SolrEvent) => {
    return {
        id: solr.id,
        ispublic: false,
        members: solr.members_ss || [],
        name: solr.name_t
    };
};

export const uploadEvent = async (events: SolrEvent[]) => {
    const init: RequestInit = {
        body:    JSON.stringify(events),
        headers: { "Content-Type": "application/json" },
        method: "post"
    };

    const url = baseUrl + collections.events + `update?commit=true`;

    fetch(url, init);
};

export const uploadNotification = async (notifications: SolrNotif[]) => {
    const init: RequestInit = {
        body: JSON.stringify(notifications),
        headers: { "Content-Type": "application/json" },
        method: "post"
    }

    const url = baseUrl + collections.notifications + `update?commit=true`;

    fetch(url, init);
};

export const notificationsForUser = async (userId: string) => {
    const userfilter = `userid_s:(${userId})`;

    const query = `query?q=${userfilter}&fl=id,userid_s,itemid_s,category_i`;

    const url = baseUrl + collections.notifications + query;

    const res = await fetch(url);

    return readStream(res.body);
};

export const solrNotifToNotification = (notif: SolrNotif) => {
    const {
        id,
        userid_s,
        itemid_s,
        category_i
    } = notif;

    return {
        id,
        userid: userid_s,
        itemid: itemid_s,
        category: NotificationCategory[category_i]
    }
};

enum NotificationCategory {
    FRIEND_REQ,
    EVENT_INVITE
}

export const eventsWithMember = async (memberId: string) => {
    const memberFilter = `members_ss:(${memberId})`;

    const nameFilter = `name_t:[* TO *]`;

    const query = `query?q=${memberFilter} AND ${nameFilter}`;

    const url = baseUrl + collections.events + query;

    const res = await fetch(url);

    return readStream(res.body);
};

export const removeSolrNotif = async (id: string) => {
    const removeReq = {
        delete: { id }
    };

    const init: RequestInit = {
        body: JSON.stringify(removeReq),
        headers: { "Content-Type": "application/json" },
        method: "post"
    }

    const url = baseUrl + collections.notifications + `update?commit=true`;

    return fetch(url, init)
};

export const eventsNearPoint = async (input: DistAndCoords) => {
    const { dist, coords } = input;
    const { lat, lon } = coords;

    const url =
    baseUrl +
    collections.events +
    `search&q=*:*
    &fq={!geofilt}
    &sfield=store
    &pt=${lat},${lon}
    &d=${dist}
    &sort=geodist() asc`;

    fetch(url).then((res) => {
        readStream(res.body);
    });
};
