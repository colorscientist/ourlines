import { User } from "ourlineslib";
import { Mutation } from "./resolvers/Mutation";
import { Query } from "./resolvers/Query";

export const resolvers = {
    Mutation,
    Query,
    User: {
        availability(user: User) {
            if (user.availability === "AVAILABLE") {
                return "AVAILABLE";
            } else if (user.availability === "IN_PUBLIC_EVENT") {
                return "IN_PUBLIC_EVENT";
            } else {
                return "HIDDEN";
            }
        }
    }
};
