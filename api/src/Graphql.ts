import {
  ApolloServer, defaultPlaygroundOptions, PlaygroundConfig
} from "apollo-server-express";
import { S3 } from "aws-sdk/clients/all";
import bodyParser from "body-parser";
import cors from "cors";
import express from "express";
import { Context } from "ourlineslib";
import pino from "pino";

import { init } from "./DB";
import { schema } from "./Schema";
import { isSolrConnected } from "./Solr";

const playground: PlaygroundConfig = {
  settings: {
    "editor.cursorShape": "line",
    ...defaultPlaygroundOptions.settings
  }
};

const logger = pino();

const PORT = 4050;

const server = new ApolloServer({
  context: async (): Promise<Context> => ({
    client: await init(logger),
    logger,
    s3: new S3()
  }),
  playground,
  schema
});

const app = express();

app.use(
  "*",
  cors(),
  bodyParser());

server.applyMiddleware({ app });

server.setGraphQLPath("/graphql");

app.listen({ port: PORT }, () => {
    const serverReadyMsg =
    `🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`;

    logger.info(serverReadyMsg);
    isSolrConnected().then((solrConnected) => {
      logger.info(`Solr connected: ${solrConnected}`);
    });
  }
);
