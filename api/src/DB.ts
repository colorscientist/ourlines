import { Client } from "cassandra-driver";
import { Logger } from "pino";

const prod = process.env.NODE_ENV === "production";

const contactPoints: string[] = prod ? [] : ["172.17.0.2", "127.0.0.1"];

// Create keyspace and tables as necessary
const keyspaceQuery = `CREATE KEYSPACE IF NOT EXISTS ourlines
WITH REPLICATION =
{ 'class' : 'SimpleStrategy', 'replication_factor' : 1 };`;

const client: Client = new Client({
    contactPoints
});

interface QueryObject {
    [s: string ]: string;
}

const executeAll = (queryObject: QueryObject) => {
    return Promise.all(
        Object.values(queryObject)
        .map((x) => client.execute(x))
    );
};


const typeQueries: QueryObject = {
    picture_type_query: `
    CREATE TYPE IF NOT EXISTS
    ourlines.picture (
    key text, bucket text,
    mimetype text, encoding text );`
};

const tableQueries: QueryObject = {
    events_table_query: `
    CREATE TABLE IF NOT EXISTS
    ourlines.events
    ( id uuid PRIMARY KEY, name text, location text,
        address text, members set<uuid>, time timestamp,
        createdAt timestamp, picture frozen<picture>,
        tags list<text>, owner uuid, ispublic boolean );`,
    invalid_tokens_query: `
    CREATE TABLE IF NOT EXISTS
    ourlines.invalid_tokens
    ( jwt text PRIMARY KEY );
    `,
    notifications_table_query: `
    CREATE TABLE IF NOT EXISTS
    ourlines.notifications
    ( id uuid PRIMARY KEY,
        userid uuid, category tinyint, read boolean, itemid uuid );`,
    users_table_query: `
    CREATE TABLE IF NOT EXISTS
    ourlines.users
    ( id uuid PRIMARY KEY, name text, email text,
        availability text, picture frozen<picture>,
        interests set<text>, friends set<uuid>,
        friendRequests set<uuid>, muted set<uuid>, password text );`,
    email_blasts_table_query: `
    CREATE TABLE IF NOT EXISTS
    ourlines.blasts
    ( id uuid PRIMARY KEY, name text )`
};

const mvQueries: QueryObject = {
    events_by_time_mv_query: `
    CREATE MATERIALIZED VIEW IF NOT EXISTS ourlines.events_by_time
    AS SELECT
    name, location, address, members, time, createdAt, picture, tags
    FROM ourlines.events
    WHERE id IS NOT NULL AND time IS NOT NULL
    PRIMARY KEY (id, time)
    WITH caching = { 'keys' : 'ALL', 'rows_per_partition' : '100' }
       AND comment = 'Based on table events' ;`,
    notifications_by_user_mv_query: `
    CREATE MATERIALIZED VIEW IF NOT EXISTS
    ourlines.notifications_by_user
    AS SELECT id, userid, category, read, itemid
    FROM ourlines.notifications
    WHERE id IS NOT NULL AND userid IS NOT NULL
    PRIMARY KEY (userid, id)
    WITH caching = { 'keys' : 'ALL', 'rows_per_partition' : '100' }
       AND comment = 'Based on table notifications' ;`,
    users_by_email_mv_query: `
    CREATE MATERIALIZED VIEW IF NOT EXISTS ourlines.users_by_email
    AS SELECT *
    FROM ourlines.users
    WHERE email IS NOT NULL AND id IS NOT NULL
    PRIMARY KEY (email, id)
    WITH caching = { 'keys' : 'ALL', 'rows_per_partition' : '100' }
        AND comment = 'Based on table users' ;`,
    blasts_by_name_mv_query: `
    CREATE MATERIALIZED VIEW IF NOT EXISTS ourlines.blasts_by_name
    AS SELECT *
    FROM ourlines.blasts
    WHERE name IS NOT NULL AND id IS NOT NULL
    PRIMARY KEY (name, id)
    WITH caching = { 'keys' : 'ALL', 'rows_per_partition' : '100' }
    AND comment = 'Based on table users' ;`
};

export const init = async (logger: Logger): Promise<Client> => {
    try {
        await client.connect()
        .then(() => client.execute(keyspaceQuery))
        .then(() => executeAll(typeQueries))
        .then(() => executeAll(tableQueries))
        .then(() => executeAll(mvQueries));
    } catch (e) {
        logger.error(e);
    }

    return client;
};
