import { hashAndSalt, verifyPass } from "./Crypto";

it("correctly verifies password", () => {
    const correct = "password";

    hashAndSalt(correct).then((hashed) => {
        verifyPass(correct, hashed).then((result) => {
            expect(result).toBe(true);
        });
    // tslint:disable-next-line:no-console
    }).catch((e) => console.log(e));
});

it("rejects incorrect password", () => {
    const correct = "password";
    const incorrect = "banana";

    hashAndSalt(correct).then((hashed) => {
        verifyPass(incorrect, hashed).then((result) => {
            expect(result).toBe(false);
        });
    // tslint:disable-next-line:no-console
    }).catch((e) => console.log(e));
});
