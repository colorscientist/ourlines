import { importSchema } from "graphql-import";
import { makeExecutableSchema } from "graphql-tools";

import { resolvers } from "./Resolvers";

const typeDefs = importSchema(__dirname + "/schema.graphql");

// Put together a schema
export const schema = makeExecutableSchema({
  resolvers,
  typeDefs
  });
