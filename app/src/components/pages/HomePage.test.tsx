import React, { createContext } from "react";
import { HomePage } from "./HomePage";

import { MockedProvider } from "react-apollo/test-utils";
import { NavigationParams, NavigationRoute, NavigationScreenProp } from "react-navigation";
import renderer from "react-test-renderer";
import { guest } from "../contexts/LoginContext";

it("renders without crashing", () => {
  const ContextMock = jest.fn(() => createContext({ jwt: "", user: guest }));

  const Context = new ContextMock();

  const NaviMock = jest.fn<NavigationScreenProp<NavigationRoute<NavigationParams>, NavigationParams>>(() => ({
    navigate: jest.fn(),
    setParams: jest.fn()
  }));

  const navigation = new NaviMock();

  const rendered = renderer.create(
    <MockedProvider>
      <Context.Provider value={{ jwt: "", user: guest }}>
        <HomePage navigation={navigation}/>
      </Context.Provider>
    </MockedProvider>
  ).toJSON();

  expect(rendered).toBeTruthy();
});
