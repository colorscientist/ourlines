import React, { Component } from "react";
import {
    Button,
    Image,
    Text,
    View
} from "react-native";
import FastImage from "react-native-fast-image";
import { NavigationScreenProp } from "react-navigation";

import { User } from "ourlineslib";
import {
    InjectedLoginStyleProps,
    LoginStyleConsumer
} from "../contexts/LoginStyleConsumer";
import { StyleType } from "../contexts/StyleContext";
interface Props {
    navigation: NavigationScreenProp<{}>;
}

const getImage = (
    style: StyleType,
    user: User,
    jwt?: string | null) => {

    const source = user.picture && jwt ? {
        headers: { Authorization: jwt },
        priority: FastImage.priority.normal,
        uri: user.picture.uri
        } : undefined;

    return user.picture && source ?
    (<FastImage
        style={style.bigImage}
        source={source}
    />
    ) : (
    <Image
        style={style.bigImage}
        // tslint:disable-next-line:no-require-imports
        source={require("../../../img/default-profile.png")}
    />
    );
};

const getPageContent = (
    injectedProps: InjectedLoginStyleProps,
    navigation: NavigationScreenProp<{}>) => {

    const { login, style } = injectedProps;

    const { user, jwt, signOut } = login;

    const { navigate } = navigation;

    if (!jwt || !user.id) {
        navigate("LoginPage");
    }
    const image = getImage(style, user, jwt);

    const signOutButton = signOut ? (
        <Button
            title="Sign Out"
            onPress={() => { signOut(); }}
        />
    ) : undefined;

    return (
        <View style={style.pageLayout}>
            {image}
            <Text>{user.name}</Text>
            <Text>{user.email}</Text>
            {signOutButton}
        </View>
    );
};

export class ProfilePage extends Component<Props> {
    public static navigationOptions = {
        headerTitle: "Profile",
        tabBarVisible: false
    };

    public render() {
        const { navigation } = this.props;

        return (
            <LoginStyleConsumer>
                {(injProps) => getPageContent(injProps, navigation)}
            </LoginStyleConsumer>
        );
    }
}
