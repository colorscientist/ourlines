
import React, { createContext } from "react";
import renderer from "react-test-renderer";

import {
  NavigationParams,
  NavigationRoute,
  NavigationScreenProp
} from "react-navigation";
import { guest } from "../contexts/LoginContext";
import { SettingsPage } from "./SettingsPage";

it("renders without crashing", () => {
  const ContextMock = jest.fn(() => createContext({ jwt: "", user: guest }));

  const Context = new ContextMock();

  const NaviMock = jest.fn<NavigationScreenProp<NavigationRoute<NavigationParams>, NavigationParams>>(() => ({
    navigate: jest.fn()
  }));

  const navigation = new NaviMock();

  const rendered = renderer.create(
    <Context.Provider value={{ user: guest, jwt: "" }}>
      <SettingsPage navigation={navigation} />
    </Context.Provider>
  ).toJSON();

  expect(rendered).toBeTruthy();
});
