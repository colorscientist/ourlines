import { ApolloError } from "apollo-client";
import { DocumentNode } from "graphql";
import gql from "graphql-tag";
import { FeedEntryItem } from "ourlineslib";
import React from "react";
import { Mutation, MutationFn, Query, QueryResult } from "react-apollo";
import {
  FlatList,
  Text,
  TouchableHighlight,
  View
} from "react-native";
import { NavigationScreenProp } from "react-navigation";

import {
  InjectedLoginStyleProps,
  LoginStyleConsumer
} from "../contexts/LoginStyleConsumer";

interface Props {
  navigation: NavigationScreenProp<{}>;
}

const getNotifsQuery = gql`
  query GetNotifsQuery($jwt: String!) {
    notifications(input: { jwt: $jwt }) {
      id
      category
      itemid
    }
  }
`;

interface Notification {
  id: string;
  itemid: string;
  category: string;
}

interface NotifsResponse {
  notifications: Notification[];
}

const renderItem = (
  notif: Notification,
  inj: InjectedLoginStyleProps) => {

  const getMessageContent = (
    data?: {
      users?: FeedEntryItem[];
      events?: FeedEntryItem[];
    },
    error?: ApolloError) => {
    if (data) {

      const mutString: DocumentNode = gql`
        mutation ConfirmFriendReq(
          $jwt: String!,
          $friendid: ID!,
          $requestid: ID!) {
            confirmFriendRequest(input: {
              jwt: $jwt,
              friendid: $friendid,
              requestid: $requestid
            })
        }
      `;

      const message = data.users ?
      `${data.users[0].email} has sent a friend request` :
      data.events ?
      `Invitation to ${data.events[0].name}` :
      undefined ;

      const addMutContent = (
        mut: MutationFn<any, any>
      ) => {
        return (
        <TouchableHighlight
          onPress={() => mut()}
        >
          <Text>{message}</Text>
        </TouchableHighlight>
        );
      };

      const mutVars = {
        friendid: data.users && data.users[0].id,
        jwt: inj.login.jwt,
        requestid: notif.id
      };

      return (
        <Mutation
          mutation={mutString}
          variables={mutVars}
        >
          {addMutContent}
        </Mutation>
      );
    } else if (error) {
      return (
        <Text>Error: {error.toString()}</Text>
      ) ;
    } else {
      return (
        <Text>No error</Text>
      );
    }
  };

  const queryString: DocumentNode = gql`
    query Users($ids: [ID!]!) {
      users(input: { ids: $ids }) {
        id
        name
        email
      }
    }
  `;

  return (
    <View
      style={{ flexDirection: "row" }}
    >
      <Query<{
        users?: FeedEntryItem[];
        events?: FeedEntryItem[];
        }, {
          ids: string[];
        }>
        query={queryString}
        variables={{ ids: [notif.itemid] }}
      >
        {({ data, error }) => getMessageContent(data, error)}
      </Query>
    </View>
  );
};

const getContent = (
  res: QueryResult<NotifsResponse, { jwt: string }>,
  inj: InjectedLoginStyleProps
  ) => {

  const { data, loading } = res;

  if (loading || !data) { return (<Text>Loading...</Text>); }

  return (
    <FlatList
      data={data.notifications}
      renderItem={({ item }) => renderItem(item, inj)}
      keyExtractor={(item) => item.id}
    />
  );
};

const addMutation = (inj: InjectedLoginStyleProps) => {
  if (inj.login.jwt) {
    const { jwt } = inj.login;

    return (
      <Query<NotifsResponse, { jwt: string }>
        query={getNotifsQuery}
        variables={{ jwt }}
      >
        {(res) => getContent(res, inj)}
      </Query>
    );
  } else {
    return <View />;
  }
};

export const NotifsModal = (_: Props) => {
  return (
    <View>
      <LoginStyleConsumer>
        {(inj) => addMutation(inj)}
      </LoginStyleConsumer>
    </View>
  );
};
