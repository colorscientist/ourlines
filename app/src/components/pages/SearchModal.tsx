import Fuse, { FuseOptions } from "fuse.js";
import { EventType, FeedEntryItem } from "ourlineslib";
import React, { Component } from "react";
import {
  Button,
  PermissionsAndroid,
  Platform,
  Text,
  TextInput,
  View
} from "react-native";
import { NavigationScreenProp } from "react-navigation";

import { StyleContext, StyleType } from "../contexts/StyleContext";
import { ItemSeparator } from "../Separator";

interface Props {
  navigation: NavigationScreenProp<{}>;
}

interface LocDist {
  lat?: number;
  lon?: number;
  dist?: number;
}
interface QueryTerms {
  searchTerm?: string;
  distanceTo?: LocDist;
  eventType?: EventType;
}

interface State {
  data: FeedEntryItem[];
  text: string;
  options: FuseOptions;
  returnItems: (
    items: FeedEntryItem[],
    queryTerms?: QueryTerms
    ) => void;
  distanceTo?: LocDist;
}

export class SearchModal extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    const options: FuseOptions = {
      keys: ["email", "name"],
      minMatchCharLength: 1
    };

    this.state = {
      data: this.props.navigation.getParam("data"),
      options,
      returnItems: this.props.navigation.getParam("returnItems"),
      text: ""
    };
  }

  public render() {
    return (
      <StyleContext.Consumer>
        {(styles) => this._getContent(styles)}
      </StyleContext.Consumer>
    );
  }

  private _getContent(styles: StyleType) {
    return (
      <View style={styles.pageLayout}>
        <View style={{ flex: 1 }}>
          <TextInput
            autoFocus={true}
            placeholder={"Search"}
            onChangeText={(text) => this._changeSearch(text)}
            onSubmitEditing={() => this._search(this.state.data)}
            returnKeyType={"search"}
          />
        </View>
        <ItemSeparator />
        <View style={{ flex: 1 }}>
          <Text>Distance</Text>
          <TextInput
            placeholder={"Distance"}
            onChangeText={(text) => this._setDist(text)}
            onFocus={() => this._setLoc()}
            textContentType={"location"}
            keyboardType={"numeric"}
          />
        </View>
        <ItemSeparator />
        <View style={{ flex: 1 }}>
          <Text>Group</Text>
        </View>
        <Button
          onPress={() => this._search(this.state.data)}
          title="Search"
        />
      </View>
    );
  }

  private async _setLoc() {
    const _setCoordinates = () => {
      navigator.geolocation.getCurrentPosition((loc) => {
        this.setState({
          distanceTo: {
            lat: loc.coords.latitude,
            lon: loc.coords.longitude
          }
        });
      });
    };

    if (Platform.OS === "android") {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      );

      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        _setCoordinates();
      }
    } else if (Platform.OS === "ios") {
      navigator.geolocation.requestAuthorization();

      _setCoordinates();
    }
  }

  private async _setDist(text: string) {
    if (Number(text)) {
      this.setState((prevState) => {
        return {
          distanceTo: {
            dist: Number(text),
            ...prevState.distanceTo
          }
        };
      });
    }
  }

  private _search(searchItems: FeedEntryItem[]) {
    const fuse = this.state.distanceTo && searchItems[0].location ?
      new Fuse(searchItems.filter((x) => {
        this._filterByDist(x);
      }), this.state.options) :
      new Fuse(searchItems, this.state.options);

    this.state.returnItems(fuse.search(this.state.text));

    this.props.navigation.goBack();
  }

  private _changeSearch(text: string) {
    this.setState({ text });
  }

  private _filterByDist(item: FeedEntryItem): boolean {
    if (
      this.state.distanceTo &&
      this.state.distanceTo.dist &&
      this.state.distanceTo.lat &&
      this.state.distanceTo.lon &&
      item.location &&
      item.location.coordinates) {
      return this._distance(
        item.location.coordinates.lat,
        item.location.coordinates.lon,
        this.state.distanceTo.lat,
        this.state.distanceTo.lon) > this.state.distanceTo.dist;
    }

    return false;
  }

  private _distance(
    lat1: number, lon1: number, lat2: number, lon2: number
    ) {
    const deg2rad = Math.PI / 180;
    lat1 *= deg2rad;
    lon1 *= deg2rad;
    lat2 *= deg2rad;
    lon2 *= deg2rad;
    const diam = 12742; // Diameter of the earth in km (2 * 6371)
    const dLat = lat2 - lat1;
    const dLon = lon2 - lon1;
    const a = (
      (1 - Math.cos(dLat)) +
      (1 - Math.cos(dLon)) * Math.cos(lat1) * Math.cos(lat2)
    ) / 2;

    return diam * Math.asin(Math.sqrt(a));
  }
}
