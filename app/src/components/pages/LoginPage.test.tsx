import React, { createContext } from "react";
import { NavigationParams, NavigationRoute, NavigationScreenProp } from "react-navigation";
import renderer from "react-test-renderer";

import { guest } from "../contexts/LoginContext";
import { LoginPage } from "./LoginPage";

it("renders without crashing", () => {
  const ContextMock = jest.fn(() => createContext({ jwt: "", user: guest }));

  const Context = new ContextMock();

  const NaviMock = jest.fn<NavigationScreenProp<NavigationRoute<NavigationParams>, NavigationParams>>(() => ({
    navigate: jest.fn()
  }));

  const navigation = new NaviMock();

  const rendered = renderer.create(
  <Context.Provider value={{ jwt: "", user: guest }}>
    <LoginPage navigation={navigation} />
  </Context.Provider>);

  expect(rendered).toBeTruthy;
});
