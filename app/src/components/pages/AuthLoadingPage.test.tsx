import React, { createContext } from "react";
import {
  NavigationParams,
  NavigationRoute,
  NavigationScreenProp
} from "react-navigation";
import renderer from "react-test-renderer";

import { guest } from "../contexts/LoginContext";
import { AuthLoadingPage } from "./AuthLoadingPage";

type NaviPropMock = NavigationScreenProp<
  NavigationRoute<NavigationParams>,
  NavigationParams
>;

it("renders without crashing", () => {
  const ContextMock =
  jest.fn(() => createContext({ jwt: "", user: guest }));

  const Context = new ContextMock();

  const NaviMock =
  jest.fn<NaviPropMock>(() => ({
    addListener: jest.fn(),
    navigate: jest.fn()
  }));

  const navigation = new NaviMock();

  const rendered = renderer.create(
  <Context.Provider value={{ jwt: "", user: guest }}>
    <AuthLoadingPage navigation={navigation} />
  </Context.Provider>);

  expect(rendered).toBeTruthy();
});
