import React, { Component } from "react";
import { NavigationScreenProp } from "react-navigation";

import { EventsFeed } from "../feeds/EventsFeed";
import { headerLeft, headerRight } from "../Header";

interface Props {
    navigation: NavigationScreenProp<{}>;
}

export class EventsFeedPage extends Component<Props> {
    public static navigationOptions =
    ({ navigation }: { navigation: NavigationScreenProp<{}> }) => {
        return {
          headerLeft: headerLeft(),
          headerRight: headerRight(
            {
                navigation,
                setFilters: navigation.getParam("setFilters")
            })
        };
    }

    constructor(props: Props) {
        super(props);
    }

    public render() {
        return (
            <EventsFeed
                navigation={this.props.navigation}
                filters={[]}
            />
        );
    }
}
