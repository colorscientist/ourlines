import gql from "graphql-tag";
import { EventType, makeEventName, User } from "ourlineslib";
import React, { Component } from "react";
import { Mutation, MutationFn, MutationResult } from "react-apollo";
import {
    Button,
    PermissionsAndroid,
    Picker,
    Platform,
    Text,
    TextInput,
    View
} from "react-native";
import MapView, { Marker } from "react-native-maps";
import { NavigationScreenProp } from "react-navigation";

import {
  InjectedLoginStyleProps,
  LoginStyleConsumer
} from "../contexts/LoginStyleConsumer";

const createEventMutationString = gql`
  mutation CreateEvent(
      $jwt: String!,
      $name: String!,
      $ownerId: ID!,
      $members: [ID!]!,
      $ispublic: Boolean!) {
    createEvent(input: {
            jwt: $jwt,
            name: $name,
            ownerId: $ownerId,
            members: $members,
            ispublic: $ispublic
            })
  }`;

interface State {
    name: string;
    ispublic: boolean;
    description: string;
    members?: User[];
    eventType: EventType;
    pageLoaded: boolean;
    loc?: {
      lat: number;
      lon: number;
    };
}

export interface Props {
    navigation: NavigationScreenProp<{}>;
    members: string[];
}

interface CreateEventVariables {
    name: string;
    ownerId: string;
    jwt: string;
    members?: string[];
    ispublic: boolean;
}

class CreateEventMutation extends Mutation<
{}, CreateEventVariables> {}

export class CreateEventPage extends Component<Props, State> {
  public static navigationOptions = () => {
      return {
          headerTitle: "Create Event",
          tabBarVisible: true
      };
  }

  constructor(props: Props) {
      super(props);

      this.state = {
          description: "",
          eventType: EventType.PersonalMeetup,
          ispublic: false,
          name: "",
          pageLoaded: false
      };
  }

  public componentDidMount() {
    const item: User | User[] | undefined =
    this.props.navigation.getParam("item") ||
    this.props.navigation.getParam("selectedUsers");

    this._setLoc();

    if (item) {
        this.setState((prevState) => {
            const members = prevState.members || [];

            return {
                members: members.concat(item)
            };
        });
    }
  }

  public render() {
    return (
        <LoginStyleConsumer>
        {(injected) => this._addMutation(injected)}
        </LoginStyleConsumer>
    );
  }

  private _addMutation(injectedProps: InjectedLoginStyleProps) {
    const createEventMutationChild = (
      createEvent: MutationFn<{}, CreateEventVariables>,
      res: MutationResult<{}>) =>
    this._getContent(createEvent, res, injectedProps);

    return (
      <CreateEventMutation mutation={createEventMutationString}>
      {createEventMutationChild}
      </CreateEventMutation>
    );
  }

  private _getContent(
    createEvent: MutationFn<{}, CreateEventVariables>,
    res: MutationResult<{}>,
    injectedProps: InjectedLoginStyleProps) {
      const { data, error, loading } = res;

      const { navigate } = this.props.navigation;

      if (error) { throw new Error(error.toString()); }
      if (loading) { return (<Text>Loading...</Text>); }
      if (data) {
        navigate("HomePage");
      }

      const { style, login } = injectedProps;

      const { name, ispublic } = this.state;

      const { user } = login;

      const membersDisplay = this.state.members ?
      [user, ...this.state.members].map(
        (member) => member.email).join(", ") :
        user.email;

      const onChangeEventDesc = (text: string) =>
      this.setState({ description: text });

      const ownerDisplay = user ?
      user.email :
      undefined;

      const members = this.state.members ?
      [ user.id, ...this.state.members.map((x) => x.id)] :
      [ user.id ];

      const owner = user.id;

      const eventName = name ||
      this.state.members && makeEventName(
        this.state.members,
        this.state.eventType) ||
      "Event";

      const onSubmit = () => {
        if (login.jwt) {
          createEvent(
            { variables:
              {
                ispublic,
                jwt: login.jwt,
                members,
                name: eventName,
                ownerId: owner
              }
            }
          );
        }
      };

      const initialRegion = this.state.loc && {
        latitude: this.state.loc.lat,
        latitudeDelta: 0.1822,
        longitude: this.state.loc.lon,
        longitudeDelta: 0.0821
      };

      const map = initialRegion && (
        <MapView
          initialRegion={initialRegion}
          style={{ height: 300, width: 300 }}
          showsPointsOfInterest={false}
        >
        <Marker
          coordinate={initialRegion}
          pinColor={"#73ff00"}
        />
        </MapView>
      );

      return (
          <View style={style.pageLayout}>
              <Picker
                selectedValue={ispublic}
                style={style.picker}
                onValueChange={(x) => this.setState({ispublic: x})}
              >
                  <Picker.Item label="Public" value={true} />
                  <Picker.Item label="Private" value={false} />
              </Picker>
              <TextInput
                style={style.textInput}
                placeholder={"Event Name"}
                onChangeText={(text) => this.setState({name: text})}
                autoFocus={true}
              />
              <TextInput
                style={style.textInput}
                placeholder={"Event Descriptions"}
                onChangeText={onChangeEventDesc}
              />
              <Text>Owner: {ownerDisplay}</Text>
              <Text>Going: {membersDisplay}</Text>
              {map}
              <Button
                title={"Submit Event"}
                onPress={onSubmit}
              />
          </View>
      );
  }

  private async _setLoc() {
    const _setCoordinates = () => {
      navigator.geolocation.getCurrentPosition((loc) => {
        this.setState({
          loc: {
            lat: loc.coords.latitude,
            lon: loc.coords.longitude
          }
        });
      });
    };

    if (Platform.OS === "android") {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      );

      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        _setCoordinates();
      }
    } else if (Platform.OS === "ios") {
      navigator.geolocation.requestAuthorization();

      _setCoordinates();
    }
  }
}
