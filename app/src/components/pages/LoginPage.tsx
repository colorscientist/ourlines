import React, { Component } from "react";
import {
    Button,
    KeyboardAvoidingView,
    Text,
    TextInput,
    View
} from "react-native";
import { NavigationScreenProp } from "react-navigation";

import { LoginState } from "../contexts/LoginContext";
import {
    InjectedLoginStyleProps,
    LoginStyleConsumer
} from "../contexts/LoginStyleConsumer";

export interface Props {
    navigation: NavigationScreenProp<{}>;
}

interface State {
    email: string;
    password: string;
    hasError?: boolean;
}

export class LoginPage extends Component<Props, State> {
    public static navigationOptions = {
        headerTitle: "Login",
        tabBarVisible: false
    };

    constructor(props: Props) {
        super(props);

        this.state = {
            email: "",
            password: ""
        };
    }

    public componentDidCatch() {
        this.setState({ hasError: true });
    }

    public render() {
        return (
        <LoginStyleConsumer>
            {(injected) => this._getContent(injected)}
        </LoginStyleConsumer>
        );
    }

    private _getContent(injectedProps: InjectedLoginStyleProps) {
        const { style, login } = injectedProps;

        const loginError = (
            <Text>Failed to log in, please try again</Text>
            );

        return (
        <KeyboardAvoidingView
            style={style.pageLayout}
            behavior="padding"
        >
            <View style={style.inputLayout}>
                <TextInput
                    style={style.textInput}
                    placeholder="email@example.com"
                    onChangeText={(text) => this._changeEmail(text)}
                    autoCorrect={false}
                    autoFocus={true}
                    keyboardType={"email-address"}
                    returnKeyType={"next"}
                    textContentType={"emailAddress"}
                />
                <TextInput
                    style={style.textInput}
                    placeholder="password"
                    onChangeText={(text) => this._changePassword(text)}
                    secureTextEntry={true}
                    autoCorrect={false}
                    onSubmitEditing={() => this._tryLogIn(login)}
                    textContentType={"password"}
                />
            </View>
            <View style={style.buttonGroup}>
                <Button
                    onPress={() => this._trySignUp(login)}
                    title={"Sign Up"}
                />
                <Button
                    onPress={() => this._tryLogIn(login)}
                    title={"Log In"}
                />
                {this.state.hasError ? loginError : undefined}
            </View>
        </KeyboardAvoidingView>
        );
    }

    private _changeEmail(text: string) {
        const email = text.trim().toLowerCase();

        this.setState({ email });
    }

    private _changePassword(password: string) {
        this.setState({ password });
    }

    private async _tryLogIn(login: LoginState) {
        const { email, password } = this.state;

        const { logIn } = login;

        if (logIn) {
            try {
                const res = await logIn({variables: {email, password}});

                if (res && res.data && res.data.logIn.jwt) {
                    this.props.navigation.navigate("HomePage");
                }
            } catch (e) {
                console.log(e);
            }
        }
    }

    private async _trySignUp(login: LoginState) {
        const { email, password } = this.state;

        const { signUp } = login;

        if (signUp) {
            try {
                const res = await signUp(
                    {variables: {email, password}});

                if (res && res.data && res.data.signUp.jwt) {
                    this.props.navigation.navigate("HomePage");
                }
            } catch (e) {
                console.log(e);
            }
        }
    }
}
