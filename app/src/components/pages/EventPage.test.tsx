import React from "react";
import { EventPage, EventQuery } from "./EventPage";

import { MockedProvider } from "react-apollo/test-utils";
import renderer from "react-test-renderer";

const mocks = [
    {
      request: {
        query: EventQuery,
        variables: {
          userid: "09333a71-994d-4b77-9b3c-f5c7e61e0e6f"
        }
      },
      result: {
        data: {
          user: { id: "09333a71-994d-4b77-9b3c-f5c7e61e0e6f", name: "Weekly Coffee" }
        }
      }
    }
  ];

it("renders without crashing", () => {

  const rendered = renderer.create(
    <MockedProvider mocks={mocks} addTypename={false}>
      <EventPage />
    </MockedProvider>
  ).toJSON();
  expect(rendered).toBeTruthy();
});
