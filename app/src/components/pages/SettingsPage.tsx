import React, { Component } from "react";
import { View } from "react-native";
import { NavigationScreenProp } from "react-navigation";

import { deleteAccount, setAvailable } from "../AccountModifiers";
import { LoginState } from "../contexts/LoginContext";
import {
    InjectedLoginStyleProps,
    LoginStyleConsumer
} from "../contexts/LoginStyleConsumer";
import { StyleType } from "../contexts/StyleContext";
import { headerLeft } from "../Header";

interface Props {
    navigation: NavigationScreenProp<{}>;
    style?: StyleType;
    login?: LoginState;
}

export class SettingsPage extends Component<Props> {
    public static navigationOptions = () => {
        return {
            headerLeft: headerLeft(),
            tabBarVisible: true
        };
    }

    constructor(props: Props) {
        super(props);
    }

    public render() {
        return (
            <LoginStyleConsumer>
                {this._getContent}
            </LoginStyleConsumer>
        );
    }

    private _getContent = (props: InjectedLoginStyleProps) => {
        return (
            <View style={props.style.pageLayout}>
                <View style={{ justifyContent: "space-around" }}>
                    {setAvailable(props)}
                    {deleteAccount(props, this.props.navigation)}
                </View>
            </View>
        );
    }
}
