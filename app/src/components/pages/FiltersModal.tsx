import React, { Component } from "react";
import {
  Button,
  FlatList,
  StyleProp,
  Text,
  TouchableHighlight,
  View,
  ViewStyle
} from "react-native";
import { NavigationScreenProp } from "react-navigation";

interface FilterProps {
  navigation: NavigationScreenProp<{}>;
  filters: string[];
}

const filterItem = (
  { item }: { item: { key: string } },
  setFilters: (filter: string) => void) => (
  <TouchableHighlight onPress={() => {setFilters(item.key); }}>
    <Text>{item.key}</Text>
  </TouchableHighlight>
);

export class FiltersModal extends Component<FilterProps> {
  public static navigationOptions: {
    tabBarVisible: false
  };

  public render() {
    const setFilters: (filter: string) => void =
    this.props.navigation.getParam("setFilters");

    const viewStyle: StyleProp<ViewStyle> = {
      alignItems: "center",
      flex: 1,
      justifyContent: "center"
    };

    return (
      <View style={viewStyle}>
        <FlatList
          data={[{key: "some"}, {key: "sample"}, {key: "filters"}]}
          renderItem={(item) => filterItem(item, setFilters)}
        />
        <Button
          onPress={() => this.props.navigation.goBack()}
          title="Dismiss"
        />
      </View>
    );
  }
}
