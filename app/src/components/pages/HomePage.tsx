import React, { Component } from "react";
import {
  InteractionManager,
  PermissionsAndroid,
  Platform,
  View
} from "react-native";
import Contacts, { Contact } from "react-native-contacts";
import { NavigationScreenProp } from "react-navigation";

import { FeedEntryItem, User } from "ourlineslib";
import {
  InjectedLoginStyleProps,
  LoginStyleConsumer
} from "../contexts/LoginStyleConsumer";
import { UserFeed } from "../feeds/UsersFeed";
import { headerLeft, headerRight } from "../Header";

interface Props {
  navigation: NavigationScreenProp<{}>;
}

interface State {
  filters: string[];
  selectedUsers: User[];
  contacts: FeedEntryItem[];
}

export class HomePage extends Component<Props, State> {
  public static navigationOptions = (
    { navigation }: { navigation: NavigationScreenProp<{}> }) => {
    return {
      headerLeft: headerLeft({
        navigation,
        removeSelected: navigation.getParam("removeSelected"),
        selectedUsers: navigation.getParam("selectedUsers")
      }),
      headerRight: headerRight({
          navigation,
          removeSelected: navigation.getParam("removeSelected"),
          selectedUsers: navigation.getParam("selectedUsers"),
          setFilters: navigation.getParam("setFilters")
      })
    };
  }

  constructor(props: Props) {
    super(props);

    this.state = {
      contacts: [],
      filters: [],
      selectedUsers: []
    };
  }

  public componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      // this._getContacts();

      this.props.navigation.setParams(
        {
          removeSelected: () =>
            this.state.selectedUsers.map(
              (x) => this._removeSelectedUser(x)),
          setFilters: this.setFilters.bind(this)
        });
    });
  }

  public setFilters(filter: string) {
    this.setState(
      (prevState) => {
        return { filters: prevState.filters.concat(filter)};
      });
  }

  public _addSelectedUser(user: User) {

    this.setState((prevState) => {
      return {
        selectedUsers: prevState.selectedUsers.concat(user)
      };
    }, () => {
      this.props.navigation.setParams(
        { selectedUsers: this.state.selectedUsers }
      );
    });
  }

  public _removeSelectedUser(user: User) {
    this.setState((prevState) => {
      const index = prevState.selectedUsers.indexOf(user);
      if (index > -1) {
        return {
          selectedUsers: prevState.selectedUsers.filter(
            (selected) => selected.id !== user.id)
        };
      } else {
        return {
          selectedUsers: prevState.selectedUsers
        };
      }
    }, () => {
      this.props.navigation.setParams(
        { selectedUsers: this.state.selectedUsers }
      );
    });
  }

  public render() {
    return (
      <LoginStyleConsumer>
        {this._getContent}
      </LoginStyleConsumer>
    );
  }

  public async _getContacts() {
    if (Platform.OS === "android") {
      PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS)
      .then((result) => {
        if (result === PermissionsAndroid.RESULTS.GRANTED) {
          Contacts.getAll((err, contacts) => {
            if (err) { throw err; }

            const contactList = contacts.map(
              (x) => this._makeContactToFeedEntry(x)
            );

            this.setState({ contacts: contactList });
          });
        }
      });
    } else {
      Contacts.requestPermission((permErr, result) => {
        if (!permErr && result === "authorized") {
          Contacts.getAll((err, contacts) => {
            if (err) { throw err; }

            const contactList = contacts.map(
              (x) => this._makeContactToFeedEntry(x)
            );

            this.setState({ contacts: contactList });
          });
        }
      });
    }
  }

  private _getContent = (injectedProps: InjectedLoginStyleProps) => {
    const { style } = injectedProps;

    return (
    <View style={style.pageLayout}>
      <UserFeed
        navigation={this.props.navigation}
        selectedUsers={this.state.selectedUsers}
        addSelectedUser={(user) => this._addSelectedUser(user)}
        removeSelectedUser={(user) => this._removeSelectedUser(user)}
        appendedItems={this.state.contacts}
      />
    </View>
    );
  }

  private _makeContactToFeedEntry(item: Contact): FeedEntryItem {
    const name = item.givenName +
    " " + item.middleName + " " + item.familyName;

    return {
      id: item.recordID,
      name
    };
  }
}
