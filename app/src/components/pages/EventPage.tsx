import gql from "graphql-tag";
import { Event, User } from "ourlineslib";
import React, { Component } from "react";
import {
  Mutation,
  MutationFn,
  MutationResult,
  Query,
  QueryResult
} from "react-apollo";
import { Button, Image, Text, View } from "react-native";
import FastImage from "react-native-fast-image";
import { NavigationScreenProp } from "react-navigation";

import { LoginState } from "../contexts/LoginContext";
import {
    InjectedLoginStyleProps,
    LoginStyleConsumer
} from "../contexts/LoginStyleConsumer";
import { headerLeft } from "../Header";

export const EventQuery = gql`
  query Event($id: ID!) {
    event(input: { id: $id }) {
      id
      name
      members
      ispublic
      picture {
        id
        height
        width
        mimetype
        filename
      }
    }
  }
`;

const joinEventString = gql`
  mutation joinEvent($id: ID!, $jwt: String!) {
    joinEvent(input: { id: $id, jwt: $jwt })
  }
`;

export interface Props {
  navigation?: NavigationScreenProp<{}>;
}

interface EventData {
  event: any;
}

const joinEventButton = (
  id: string,
  login: LoginState,
  members: User[]) => {

  const joinMutation = (
    mutateFn: MutationFn,
    _: MutationResult) => {
      return (
        <Button
          title={"Join Event"}
          onPress={() => mutateFn()}
        />
      );
    };

  return members.map((x) => x.id).includes(login.user.id) ? (
    <Mutation
      mutation={joinEventString}
      variables={{ id, jwt: login.jwt }}
    >
      {joinMutation}
    </Mutation>

  ) : undefined;
};

const getContent = (
  { loading, error, data }: QueryResult<EventData>,
  injected: InjectedLoginStyleProps) => {

  if (error) { return <Text>`Error! ${error.message}`</Text>; }
  if (loading || !data) { return <Text>Loading...</Text>; }

  const { style } = injected;

  const { event }: { event: Event } = data;

  const src = event.picture && {
      headers: { Authorization: "someAuthToken" },
      priority: FastImage.priority.normal,
      uri: event.picture.uri
  };

  const image = src ?
  (<FastImage
      style={style.bigImage}
      source={src}
  />) :
  (<Image
      style={style.bigImage}
      // tslint:disable-next-line:no-require-imports
      source={require("../../../img/default-profile.png")}
  />);

  return (
      <View style={style.pageLayout}>
          {image}
          <Text>{event.name}</Text>
          <Text>{"Users: "}{event.members}</Text>
          {joinEventButton(event.id, injected.login, event.members)}
      </View>
  );
};

const addInjectedProps = (result: QueryResult<EventData>) => {
  return (
  <LoginStyleConsumer>
    {(injected) => getContent(result, injected)}
  </LoginStyleConsumer>
  );
};

export class EventPage extends Component<Props> {
  public static navigationOptions = () => {
    return {
      headerLeft: headerLeft()
    };
  }

  constructor(props: Props) {
    super(props);
  }

  public render() {
    const item: Event =
    this.props.navigation && this.props.navigation.getParam("item");

    return (
      <Query query={EventQuery} variables={{ id: item.id }}>
        {addInjectedProps}
      </Query>
    );
  }
}
