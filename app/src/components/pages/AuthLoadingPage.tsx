import { User } from "ourlineslib";
import React, { Component } from "react";
import {
  ActivityIndicator,
  View
} from "react-native";
import {
  NavigationEventSubscription,
  NavigationScreenProp
} from "react-navigation";
import { LoginConsumer } from "../contexts/LoginContext";
import {
  InjectedLoginStyleProps,
  LoginStyleConsumer
} from "../contexts/LoginStyleConsumer";

interface Props {
  navigation: NavigationScreenProp<{}>;
  user?: User;
  jwt?: string | null;
}

interface State {
  checkAndNavSub?: NavigationEventSubscription;
  checkAndNavRemoveInterval?: NavigationEventSubscription;
  interval?: number;
}

class AuthLoadingPageClass extends Component<Props, State> {
  public static navigationOptions = {
    tabBarVisible: false
  };

  public componentDidMount() {
    this.setState({
      checkAndNavRemoveInterval: this.props.navigation.addListener(
        "willBlur", () => {
        this._clearInterval();
      }),
      checkAndNavSub: this.props.navigation.addListener(
        "didFocus", () => {
          this._checkAndNav();
      }),
      interval: setInterval(() => this._checkAndNav(), 100)
    });
  }

  public componentWillUnmount() {
    this._clearInterval();

    if (this.state.checkAndNavSub) {
      this.state.checkAndNavSub.remove();
    }
    if (this.state.checkAndNavRemoveInterval) {
      this.state.checkAndNavRemoveInterval.remove();
    }
  }

  public render() {
    return (
      <LoginStyleConsumer>
        {this._getContent}
      </LoginStyleConsumer>
    );
  }

  private _getContent = (props: InjectedLoginStyleProps) => {
    return (
    <View style={props.style.pageLayout}>
      <ActivityIndicator size="large" color="#003f00"/>
    </View>
    );
  }

  private _checkAndNav() {
    const { jwt, user, navigation } = this.props;

    jwt && user && user.id ?
    navigation.navigate("HomePage") :
    jwt === null ?
    navigation.navigate("LoginPage") :
    Function.prototype();
  }

  private _clearInterval() {
    if (this.state.interval) {
      clearInterval(this.state.interval);
      this.setState({ interval: undefined });
    }
  }
}

export const AuthLoadingPage = (props: Props) => {
  const MakeAuthPage = (
    { jwt, user }: { jwt?: string | null, user?: User }
  ) => (
  <AuthLoadingPageClass
    {...props}
    jwt={jwt}
    user={user}
    navigation={props.navigation}
  />
  );

  return (
  <LoginConsumer>
    {(state) => MakeAuthPage(state)}
  </LoginConsumer>
  );
};
