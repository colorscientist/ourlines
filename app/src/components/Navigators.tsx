import React from "react";
import { Image, Platform  } from "react-native";
import {
  createBottomTabNavigator,
  createStackNavigator,
  createSwitchNavigator,
  Header,
  HeaderProps,
  TabNavigatorConfig
} from "react-navigation";
import {
  createMaterialBottomTabNavigator, TabConfig
} from "react-navigation-material-bottom-tabs";

import { AuthLoadingPage } from "./pages/AuthLoadingPage";
import { CreateEventPage } from "./pages/CreateEventPage";
import { EventPage } from "./pages/EventPage";
import { EventsFeedPage } from "./pages/EventsFeedPage";
import { FiltersModal } from "./pages/FiltersModal";
import { HomePage } from "./pages/HomePage";
import { LoginPage } from "./pages/LoginPage";
import { NotifsModal } from "./pages/NotificationsModal";
import { ProfilePage } from "./pages/ProfilePage";
import { SearchModal } from "./pages/SearchModal";
import { SettingsPage } from "./pages/SettingsPage";

const tabIconStyle = {
  flex: 1,
  height: 29,
  width: 29
};

const HomeStack = createStackNavigator(
  {
    CreateEvent: CreateEventPage,
    Event: EventPage,
    FiltersModal,
    HomePage,
    NotifsModal,
    Profile: ProfilePage,
    SearchModal
  },
  {
    initialRouteName: "HomePage",
    mode: "modal",
    navigationOptions: {
      headerStyle: {
        backgroundColor: "#126b2b"
      },
      headerTintColor: "white",
      headerTitleStyle: {
        color: "white"
      }
    }
  }
);

HomeStack.navigationOptions = {
  tabBarIcon: (
  <Image
    style={tabIconStyle}
    // tslint:disable-next-line:no-require-imports
    source={require("../../img/Buttons_Home_white.png")}
  />
)
};

const SettingsStack = createStackNavigator(
  { Settings: SettingsPage }, {
    navigationOptions: {
      headerStyle: {
        backgroundColor: "#126b2b"
      },
      headerTintColor: "white",
      headerTitleStyle: {
        color: "white"
      }
    }
  });
SettingsStack.navigationOptions = {
  tabBarIcon: (
  <Image
    style={tabIconStyle}
    // tslint:disable-next-line:no-require-imports
    source={require("../../img/Button_Settings_alternative_white.png")}
  />)
};

const eventIconLoc =
"../../img/Button_Public_Event_Alternative_white.png";

const EventsFeedStack = createStackNavigator(
  { Events: EventsFeedPage }, {
    navigationOptions: {
      headerStyle: {
        backgroundColor: "#126b2b"
      },
      headerTintColor: "white",
      headerTitleStyle: {
        color: "white"
      }
    }
  });
EventsFeedStack.navigationOptions = {
  tabBarIcon: (
  <Image
    style={tabIconStyle}
    // tslint:disable-next-line:no-require-imports
    source={require(eventIconLoc)}
  />)
};

const bottomTabMap = {
  Events: EventsFeedStack,
  Home: HomeStack,
  Settings: SettingsStack
};

const bottomTabConfigBase: TabConfig & TabNavigatorConfig = {
  initialRouteName: "Home",
  navigationOptions: ({ navigation }) => {
    return {
      header: (headerProps: HeaderProps) => <Header {...headerProps} />,
      headerTintColor: "white",
      tabBarVisible: navigation.state.index === 0
    };
  },
  order: ["Home", "Events", "Settings"]
};

const bottomTabConfig = {
  ...bottomTabConfigBase,
  style: {
    backgroundColor: "#003f00"
  }
};

const materialBottomTabConfig = {
  ...bottomTabConfigBase,
  barStyle: {
    backgroundColor: "#003f00"
  }
};

export const BottomTabStack = Platform.OS === "android" ?
createMaterialBottomTabNavigator(
  bottomTabMap, materialBottomTabConfig) :
createBottomTabNavigator(bottomTabMap, bottomTabConfig);

export const AppNav = createSwitchNavigator({
  AuthLoadingPage,
  BottomTabStack,
  LoginPage
}, {
  initialRouteName: "AuthLoadingPage"
});
