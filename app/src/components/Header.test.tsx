import React from "react";
import { View } from "react-native";
import {
    NavigationParams,
    NavigationRoute,
    NavigationScreenProp
} from "react-navigation";
import renderer from "react-test-renderer";

import { headerLeft, headerRight } from "./Header";

it("renders without crashing", () => {
    const rendered = renderer.create(
        <View>
            {headerLeft()}
        </View>
    ).toJSON();

    expect(rendered).toBeTruthy();
});

it("renders without crashing", () => {
    const NaviMock =
    jest.fn<
    NavigationScreenProp<NavigationRoute<NavigationParams>,
    NavigationParams>>(() => ({
        navigate: jest.fn()
    }));

    const navigation = new NaviMock();

    const rendered = renderer.create(
        <View>
            {headerRight({ navigation })}
        </View>
    ).toJSON();

    expect(rendered).toBeTruthy();
});
