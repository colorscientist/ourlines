import gql from "graphql-tag";
import React from "react";
import { Mutation, MutationFn } from "react-apollo";
import { Alert, Button, TextInput, View } from "react-native";
import { NavigationScreenProp } from "react-navigation";

import { LoginState } from "./contexts/LoginContext";
import { InjectedLoginStyleProps } from "./contexts/LoginStyleConsumer";

const setAvailableMutation = gql`
  mutation SetAvailableMutation(
    $jwt: String!, $availability: String!) {
    setAvailability(input: {
      availability: $availability,
      jwt: $jwt
      })
}
`;

type AvailableMutFn = MutationFn<
{ success: boolean },
{ jwt: string, availability: string }>;

const avChild = (
  mutAvailable: AvailableMutFn,
  jwt: string,
  availability: string) => (
  <Button
    title="Set Available"
    onPress={() => mutAvailable({variables: {jwt, availability}})}
  />
);

export const setAvailable = (props: InjectedLoginStyleProps) => {
  if (props.login.jwt) {
    const { jwt } = props.login;

    return (
      <Mutation mutation={setAvailableMutation}>
      {(mut: AvailableMutFn) => avChild(mut, jwt, "AVAILABLE")}
      </Mutation>
    );
  } else {
    return undefined;
  }
};

type DeleteAccFn = MutationFn<
{ success: boolean },
{ jwt: string }>;

const deleteAccountMutation = gql`
  mutation DeleteAccountMutation($jwt: String!) {
    deleteAccount(input: { jwt: $jwt })
  }
`;

const delChild = (
  mut: DeleteAccFn,
  login: LoginState,
  nav: NavigationScreenProp<{}>) => {
  const handleDelAcc = () => {
      if (login.signOut && login.jwt) {
          const { jwt, signOut } = login;

          const pressOK = () =>
          mut({ variables: { jwt }})
          .then(() => signOut())
          .then(() => nav.navigate("LoginPage"));

          Alert.alert(
              "Delete account",
              "Are you sure you would like to delete your account? " +
              "This action cannot be reversed!",
              [
              {
                  style: "cancel",
                  text: "Cancel"
              },
              {
                  onPress: pressOK,
                  style: "destructive",
                  text: "OK"
              }
              ]
            );
      }
  };

  return (
      <Button
          title="Delete Account"
          onPress={() => handleDelAcc()}
          color="red"
      />
    );
};

export const deleteAccount = (
  props: InjectedLoginStyleProps,
  nav: NavigationScreenProp<{}>) => {
  if (props.login) {
      const { login } = props;

      return (
          <Mutation mutation={deleteAccountMutation}>
          {(mut: DeleteAccFn) => delChild(mut, login, nav)}
          </Mutation>
      );
  } else {
      return undefined;
  }
};

const sendFriendReqString = gql`
  mutation SendFriendRequest($jwt: String!, $friendid: ID!) {
    sendFriendRequest(input: { jwt: $jwt, friendid: $friendid })
  }
`;

const sendFriendChild = (
  mut: MutationFn,
  props: InjectedLoginStyleProps
) => {
  let id = "";

  const sendReq = () =>
  mut({ variables: { jwt: props.login.jwt, friendid: id }});

  return (
    <View>
      <TextInput
        onChangeText={(x) => id = x}
      />
      <Button
        title="Send Friend Request"
        onPress={sendReq}
      />
    </View>
  );
};

export const sendFriendRequest = (
  props: InjectedLoginStyleProps
) => {
  if (props.login) {
    return (
      <Mutation mutation={sendFriendReqString}>
      {(mut) => sendFriendChild(mut, props)}
      </Mutation>
    );
  } else {
    return undefined;
  }
};
