import { NormalizedCacheObject } from "apollo-cache-inmemory";
import { ApolloClient, ApolloError } from "apollo-client";
import { DocumentNode } from "graphql";
import gql from "graphql-tag";
import { Availability, User } from "ourlineslib";
import React, { Component, ReactNode } from "react";
import {
  ApolloConsumer,
  Mutation,
  MutationFn,
  Query
} from "react-apollo";
import {
  AsyncStorage
} from "react-native";

export const guest: User = new User(
  {
    availability: Availability.HIDDEN,
    email: "",
    id: "",
    name: "Guest"
  });

const logInMutationString = gql`
  mutation LogIn($email: String!, $password: String!) {
    logIn(input: { email: $email, password: $password }) {
      user {
        id
        email
        phone
        name
        friends
        muted
        interests
        picture {
          id
        }
        availability
        friendRequests
      }
      jwt
    }
  }
`;

const signOutMutationString = gql`
  mutation SignOut($jwt: String!) {
    signOut(input: { jwt: $jwt })
  }
`;

const signUpMutationString = gql`
  mutation SignUp($email: String!, $password: String!) {
    signUp(input: { email: $email, password: $password }) {
      user {
        id
      }
      jwt
    }
  }
`;

const currentUserQueryString = gql`
  query CurrentUserQuery($jwt: String) {
    currentUser(input: { jwt: $jwt }) {
      id
      email
      phone
      name
      friends
      muted
      interests
      picture {
        id
      }
      availability
      friendRequests
    }
  }
`;

const resetPasswordMutationString = gql`
  mutation ResetPassword($email: String!) {
    resetPassword(input: { email: $email })
  }
`;

interface LogInData {
  logIn: {
    user: User;
    jwt: string;
  };
}

interface SignUpData {
  signUp: {
    user: User;
    jwt: string;
  };
}

interface LoginOrSignupVariables {
  email: string;
  password: string;
}

type LogInVariables = LoginOrSignupVariables;
type SignUpVariables = LoginOrSignupVariables;

class SignUpMutation extends Mutation<SignUpData, SignUpVariables> {}
// tslint:disable-next-line:max-classes-per-file
class LogInMutation extends Mutation<LogInData, LogInVariables> {}

interface CurrentUserData {
  currentUser: User;
}

interface CurrentUserVariables {
  jwt: string;
}
// tslint:disable-next-line:max-classes-per-file
class CurrentUserQuery extends Query<
  CurrentUserData,
  CurrentUserVariables> {}

export interface SignOutVariables {
  jwt: string;
}

// tslint:disable-next-line:max-classes-per-file
class SignOutMutation extends Mutation<{}, SignOutVariables> {}

interface ResetPasswordVariables {
  email: string;
}

// tslint:disable-next-line:max-classes-per-file
class ResetPasswordMutation extends Mutation<
{},
ResetPasswordVariables> {}

export interface LoginState {
  user: User;
  jwt?: string | null;
  logIn?: MutationFn<LogInData, LogInVariables>;
  signUp?: MutationFn<SignUpData, SignUpVariables>;
  signOut?: MutationFn<{}, SignOutVariables>;
  resetPassword?: MutationFn<{}, ResetPasswordVariables>;
}

const LoginContext = React.createContext<LoginState>({ user: guest });

type MutationComp = typeof SignUpMutation
  | typeof LogInMutation
  | typeof ResetPasswordMutation
  | typeof SignOutMutation
  | typeof Mutation;

interface Props {
  children: Element;
  client?: ApolloClient<NormalizedCacheObject>;
}

// tslint:disable-next-line:max-classes-per-file
class LoginProviderClass extends Component<Props, LoginState> {
  constructor(props: Props) {
    super(props);

    this.state = {
      user: guest
    };
  }

  public componentDidMount() {
    AsyncStorage.getItem("jwt").then((jwt) => this.setState({ jwt }));
  }

  public render() {
    const { children } = this.props;

    if (!!this.state.jwt && !this.state.user.id) {
      const provider = () => {
        return (
          <LoginContext.Provider value={this.state}>
          {children}
          </LoginContext.Provider>
        );
      };

      return (
        <CurrentUserQuery
          query={currentUserQueryString}
          variables={{ jwt: this.state.jwt }}
          onCompleted={(data) => this._onCurrentUser(data)}
          onError={(error) => { throw new Error(error.toString()); }}
        >
          {() => provider()}
        </CurrentUserQuery>
      );
    }

    const { jwt } = this.state;

    const LoginMutations = [
      {
        LoginMutation: LogInMutation,
        mutationName: "logIn",
        mutationProps: {
          mutation: logInMutationString,
          onCompleted: (data: LogInData) => this._onLogIn(data)
        }
      },
      {
        LoginMutation: SignUpMutation,
        mutationName: "signUp",
        mutationProps: {
          children,
          mutation: signUpMutationString,
          onCompleted: (data: SignUpData) => this._onSignUp(data)
        }
      },
      {
        LoginMutation: SignOutMutation,
        mutationName: "signOut",
        mutationProps: {
          children,
          mutation: signOutMutationString,
          onCompleted: () => this._onSignOut(),
          variables: { jwt }
        }
      },
      {
        LoginMutation: ResetPasswordMutation,
        mutationName: "resetPassword",
        mutationProps: {
          children,
          mutation: resetPasswordMutationString
        }
      }
    ];

    return this._addToLoginState(
      LoginMutations,
      { ...this.state });
  }

  private _addToLoginState(
    [Head, ...Tail]: Array<{
      LoginMutation: MutationComp,
      mutationProps: {
        mutation: DocumentNode,
        onCompleted?: (data: SignUpData & LogInData) => void,
        children?: ReactNode
      },
      mutationName: string
    }> = [],
    login: LoginState): Element {

    const mutate = (
      mutation: MutationFn,
      { error }: { error?: ApolloError }) => {
        if (error) {
          throw new Error("Error in Login" + error.toString());
        }

        const newLogin: LoginState = {
          [Head.mutationName]: mutation,
          ...login
        };

        if (Tail.length === 0) {
          return (
            <LoginContext.Provider value={newLogin}>
            {Head.mutationProps.children}
            </LoginContext.Provider>
          );
        } else {
          return this._addToLoginState(Tail, newLogin);
        }
    };

    return (
      <Head.LoginMutation {...Head.mutationProps}>
        {mutate}
      </Head.LoginMutation>
    );
  }

  private _onCurrentUser = (data: CurrentUserData | {}) => {
    const {
      availability,
      email,
      friends,
      id,
      interests,
      muted,
      name,
      phone
    } = (data as CurrentUserData).currentUser;

    const user = new User(
      {
        availability,
        email,
        friends,
        id,
        interests,
        muted,
        name,
        phone
      });

    this.setState({ user });
  }

  private _onSignUp = (data: SignUpData) => {
    this.setState({ user: data.signUp.user, jwt: data.signUp.jwt });
    this._asyncStore(data.signUp.jwt);
  }

  private _onLogIn = (data: LogInData) => {
    const {
      availability,
      email,
      friends,
      id,
      interests,
      muted,
      name,
      phone
    } = data.logIn.user;

    const user = new User({
      availability,
      email,
      friends,
      id,
      interests,
      muted,
      name,
      phone
    });

    this.setState({ user, jwt: data.logIn.jwt });
    this._asyncStore(data.logIn.jwt);
  }

  private _onSignOut = () => {
    this.setState({ user: guest, jwt: "" });
    this._asyncStore("");
    if (this.props.client) {
      this.props.client.resetStore();
    }
  }

  private async _asyncStore(jwt: string) {
    await AsyncStorage.setItem("jwt", jwt);
  }
}

export const LoginProvider = (props: Props) => {
  return (
    <ApolloConsumer>
      {(client) => <LoginProviderClass client={client} {...props}/>}
    </ApolloConsumer>
  );
};

export const LoginConsumer = LoginContext.Consumer;
