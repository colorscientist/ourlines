import React from "react";
import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    bigImage: {
        borderRadius: 100,
        height: 200,
        width: 200
    },
    buttonGroup: {
        height: 100,
        justifyContent: "space-around"
    },
    header: {
        alignItems: "center",
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        paddingRight: 20,
        width: 200
    },
    headerButton: {
        height: 34,
        width: 34
    },
    inputLayout: {
        justifyContent: "space-around"
    },
    logo: {
        height: 50,
        width: 50
    },
    logoContainer: {
        alignItems: "center",
        flex: 1,
        justifyContent: "center",
        paddingLeft: 20,
        width: 150
    },
    logoContainerSelected: {
        alignItems: "center",
        flex: 1,
        justifyContent: "center",
        width: 30
    },
    pageLayout: {
        alignItems: "center",
        flex: 1,
        flexDirection: "column",
        justifyContent: "center"

    },
    picker: {
        height: 50,
        width: 150
    },
    textInput: {
        fontSize: 20,
        height: 80,
        minWidth: 300,
        textAlign: "center"
    }
});

export type StyleType = (typeof styles);

export const StyleContext = React.createContext(styles);
