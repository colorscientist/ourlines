import React from "react";

import { LoginConsumer, LoginState } from "./LoginContext";
import { StyleContext, styles, StyleType } from "./StyleContext";

export interface InjectedLoginStyleProps {
  login: LoginState;
  style: (typeof styles);
}

interface WithLoginAndStyleProps {
  children(props: InjectedLoginStyleProps): JSX.Element;
}

export class LoginStyleConsumer extends
React.Component<WithLoginAndStyleProps> {
    public render() {

      const addStyle = (style: StyleType, login: LoginState) => {
        return this.props.children({ login, style });
      };

      const addLogin = (login: LoginState) => {
        return (
          <StyleContext.Consumer>
            {(style) => addStyle(style, login)}
          </StyleContext.Consumer>
        );
      };

      return (
        <LoginConsumer>
          {addLogin}
        </LoginConsumer>
      );
    }
  }
