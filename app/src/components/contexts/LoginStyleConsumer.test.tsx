import React, { createContext } from "react";
import { Text } from "react-native";
import renderer from "react-test-renderer";

import { guest, LoginState } from "./LoginContext";
import { LoginStyleConsumer } from "./LoginStyleConsumer";
import { StyleContext, styles } from "./StyleContext";

it("renders without crashing", () => {
    const ContextMock = jest.fn(
        () => createContext({ jwt: "", user: guest }));

    const Context = new ContextMock();

    const StyleLoginText = (
        props: { style: (typeof styles), login: LoginState }) => (
        <Text>{props.style.toString() + props.login.jwt}</Text>);

    const rendered = renderer.create(
    <StyleContext.Provider value={styles}>
        <Context.Provider value={{ jwt: "", user: guest }}>
            <LoginStyleConsumer>
                {({ login, style }) => {
                    return (
                        <StyleLoginText login={login} style={style} />
                    );
                }}
            </LoginStyleConsumer>
        </Context.Provider>
    </StyleContext.Provider>
    ).toJSON();

    expect(rendered).toBeTruthy();
  });
