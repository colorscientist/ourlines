import React from "react";
import { MockedProvider } from "react-apollo/test-utils";
import { Text } from "react-native";
import renderer from "react-test-renderer";

import { User } from "ourlineslib";
import { LoginConsumer, LoginProvider } from "./LoginContext";

it("renders without crashing", () => {
  const rendered = renderer.create(
  <MockedProvider>
    <LoginProvider>
      <Text>Hallo</Text>
    </LoginProvider>
  </MockedProvider>).toJSON();
  expect(rendered).toBeTruthy();
});

it("renders without crashing", () => {
    const rendered = renderer.create(
    <MockedProvider>
        <LoginProvider>
          <LoginConsumer>
              {({ user }: { user: User}) => <Text>{user.name}</Text>}
          </LoginConsumer>
      </LoginProvider>
    </MockedProvider>).toJSON();
    expect(rendered).toBeTruthy();
  });
