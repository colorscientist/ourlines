import React from "react";
import { Text } from "react-native";
import renderer from "react-test-renderer";

import { StyleContext, styles } from "./StyleContext";

it("renders without crashing", () => {
    const text = (style: (typeof styles)) => (
        <Text>{style.toString()}</Text>);

    const rendered = renderer.create(
    <StyleContext.Provider value={styles}>
        <StyleContext.Consumer>
            {text}
        </StyleContext.Consumer>
    </StyleContext.Provider>
    ).toJSON();
    expect(rendered).toBeTruthy();
  });
