import React, { PureComponent } from "react";
import { View, ViewStyle } from "react-native";

const separatorStyle: ViewStyle = {
  backgroundColor: "#000",
  flex: 1,
  flexDirection: "row",
  height: 2
};

export class ItemSeparator extends PureComponent {
  public render() {
    return (
      <View style={separatorStyle} />
    );
  }
}
