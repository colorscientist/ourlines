import { User } from "ourlineslib";
import React from "react";
import {
    Image,
    TouchableOpacity,
    View
} from "react-native";
import {
    HeaderBackButton,
    NavigationScreenProp
} from "react-navigation";

import { LoginState } from "./contexts/LoginContext";
import { LoginStyleConsumer } from "./contexts/LoginStyleConsumer";
import { StyleContext, StyleType } from "./contexts/StyleContext";

interface HeaderProps {
    navigation: NavigationScreenProp<{}>;
    removeSelected?: () => void;
    setFilters?: (filter: string) => void;
    selectedUsers?: User[];
}

const logoLocation =
"../../img/Logo.png";

const headerLeftContent = (
  styles: StyleType,
  props?: HeaderProps) => {

  if (props && props.selectedUsers && props.selectedUsers.length > 0) {
    const onPressRemove = () => props.removeSelected ?
    props.removeSelected() : Function.prototype();

    return (
      <View style={styles.logoContainer} >
      <HeaderBackButton
        onPress={onPressRemove}
      />
      </View>
    );
  } else {
    return (
      <View style={styles.logoContainer}>
        <Image
          source={require(logoLocation)}
          style={styles.logoContainer}
        />
      </View>
    );
  }
};

export const headerLeft = (props?: HeaderProps) => {
    return (
        <StyleContext.Consumer>
            {(style) => headerLeftContent(style, props)}
        </StyleContext.Consumer>
    );
};

const navToFilter = (
    navigation: NavigationScreenProp<{}>,
    setFilters?: (filter: string) => void) => {
    navigation.navigate(
        "FiltersModal",
        { setFilters }
    );
};

const filterButtonLocation =
"../../img/Button_Filters_white.png";

const notificationButtonLocation =
"../../img/Button_Notifications_alternative_white.png";

const createEventButtonLocation =
"../../img/Button_Public_Event_Alternative_white.png";

const filters = (styles: StyleType, props: HeaderProps) => {
    return (
    <TouchableOpacity
        style={styles.headerButton}

        onPress={() => navToFilter(props.navigation, props.setFilters)}
    >
        <Image
            style={styles.headerButton}
            source={require(filterButtonLocation)}
        />
    </TouchableOpacity>
    );
};

const notifs = (styles: StyleType, props: HeaderProps) => {
  return (
    <TouchableOpacity
      style={styles.headerButton}
      onPress={() => props.navigation.navigate("NotifsModal")}
    >
        <Image
          style={styles.headerButton}
          source={require(notificationButtonLocation)}
        />
    </TouchableOpacity>
  );
};

const profile = (
  styles: StyleType,
  props: HeaderProps,
  login: LoginState) => {

  const picStyle = {
    borderColor: "white",
    borderRadius: 34,
    borderWidth: 2,
    maxHeight: 34,
    maxWidth: 34
  };

  return (
    <TouchableOpacity
      style={styles.headerButton}
      onPress={() => profileNav(props, login.jwt)}
    >
        <Image
          style={picStyle}
          // tslint:disable-next-line:no-require-imports
          source={require("../../img/default.png")}
        />
    </TouchableOpacity>
  );
};

const profileNav = (props: HeaderProps, jwt?: string | null) => {
  if (jwt) {
    props.navigation.navigate("Profile");
  } else {
    props.navigation.navigate("Auth");
  }
};

const createEventNav = (props: HeaderProps) => {
  if (props.removeSelected) {
    props.removeSelected();
  }

  props.navigation.navigate(
      "CreateEvent",
      { selectedUsers: props.selectedUsers }
    );
};

const headerRightContent = (
    login: LoginState, styles: StyleType, props: HeaderProps) => {
      if (props.selectedUsers && props.selectedUsers.length > 0) {
        return (
          <View style={[styles.header, { width: 50 }]}>
            <TouchableOpacity
              style={styles.headerButton}
              onPress={() => createEventNav(props)}
            >
              <Image
                style={styles.headerButton}
                source={require(createEventButtonLocation)}
              />
            </TouchableOpacity>
          </View>
        );
      } else {
        return (
          <View style={styles.header}>
            {filters(styles, props)}
            {notifs(styles, props)}
            {profile(styles, props, login)}
          </View>
      );
    }
};

export const headerRight = (props: HeaderProps) => {
    return (
      <LoginStyleConsumer>
      {({ login, style }) => headerRightContent(login, style, props)}
      </LoginStyleConsumer>
    );
};
