import React, { PureComponent } from "react";
import { FlatList, Text, TextInput, View } from "react-native";

interface State {
  text: string;
}

export class LocationSuggestor extends PureComponent<{}, State> {
  public render() {
    return (
      <View>
        <TextInput
          placeholder={"Location"}
          onChangeText={(text) => this.setState({ text })}
        />
        <FlatList
          data={this._filterLocations(this.state.text)}
          renderItem={({ item }) => this._renderItem(item)}
        />
      </View>
    );
  }

  private _filterLocations(text: string) {
    return [text];
  }

  private _renderItem(item: string) {
    return (
      <View>
        <Text>{item}</Text>
      </View>
    );
  }
}
