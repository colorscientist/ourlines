import { FeedEntryItem, isEvent, isUser } from "ourlineslib";
import React, { PureComponent } from "react";
import {
    Image,
    StyleProp,
    Text,
    TouchableHighlight,
    View,
    ViewStyle
} from "react-native";
import FastImage, { FastImageSource } from "react-native-fast-image";
import { NavigationScreenProp } from "react-navigation";

export interface Props<T> {
    item: T;
    navigation?: NavigationScreenProp<{}>;
    detailsPage?: string;
    addSelected?: (selected: T) => void;
    removeSelected?: (selected: T) => void;
    selected?: T[];
    multiselect?: boolean;
}

export class FeedEntry<T extends FeedEntryItem>
extends PureComponent<Props<T>> {
    constructor(props: Props<T>) {
        super(props);
    }

    public render() {
        const {
            addSelected,
            item,
            selected,
            multiselect,
            navigation,
            detailsPage,
            removeSelected
        } = this.props;

        const location =
        isEvent(item) && (<Text>{item.location}</Text>);

        const availability =
        item.availability === "AVAILABLE" ?
        (<Text>Available</Text>) :
        item.availability === "IN_PUBLIC_EVENT" ?
        (<Text>In public event</Text>) :
        undefined;

        const name = item.name ?
        item.name : item.email ?
        item.email : "";

        const checkbox =
        selected ? (selected.includes(item) ?
        (<Text>Check Box</Text>) : (<Text>Empty Box</Text>)) :
        Function.prototype();

        const imageSource: FastImageSource = {
            headers: { Authorization: "someAuthToken" },
            priority: FastImage.priority.normal,
            uri: item.picture && item.picture.uri
        };

        const image = (isUser(item) || isEvent(item)) && item.picture ?
        (<FastImage
            style={{ borderRadius: 34 }}
            source={imageSource}
        />) :
        (<Image
            style={{ borderRadius: 34 }}
            // tslint:disable-next-line:no-require-imports
            source={require("../../../img/default-feed.png")}
        />);

        const press = () => {
            if (multiselect && selected) {
                if (selected.includes(item) && removeSelected) {
                    removeSelected(item);
                } else if (addSelected) {
                    addSelected(item);
                }
            } else if (detailsPage && navigation) {
                navigation.navigate(
                    detailsPage,
                    {
                        item
                    });
            }
        };

        const longPress = () => {
            if (multiselect && addSelected) {
                addSelected(item);
            }
        };

        const viewStyle: StyleProp<ViewStyle> = {
            alignItems: "center",
            flex: 1,
            flexDirection: "row",
            justifyContent: "flex-start",
            padding: 20
        };

        const infoStyle: StyleProp<ViewStyle> = {
            flexDirection: "column",
            justifyContent: "space-around",
            marginLeft: 25
        };

        const info = (
        <View style={infoStyle}>
            <Text
                style={{ fontWeight: "bold", fontSize: 16 }}
            >
                {name}
            </Text>
            {location}
            {availability}
        </View>
        );

        return (
        <TouchableHighlight
            onPress={() => press()}
            onLongPress={() => longPress()}
            underlayColor={"#499a56"}
            style={{ flex: 1, marginLeft: 17, paddingRight: 0 }}
        >
            <View style={viewStyle}>
                {checkbox}
                {image}
                {info}
            </View>
        </TouchableHighlight>
        );
    }
}
