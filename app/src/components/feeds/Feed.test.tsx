import React, { createContext } from "react";
import { MockedProvider } from "react-apollo/test-utils";
import {
  NavigationParams,
  NavigationRoute,
  NavigationScreenProp
} from "react-navigation";
import renderer from "react-test-renderer";

import { guest } from "../contexts/LoginContext";
import { FeedEntries } from "./EventsFeed";
import { Feed } from "./Feed";

const mocks = [
  {
    request: {
      query: FeedEntries,
      variables: {
        jwt: "aa"
      }
    },
    result: {
      data: {
        events: [
          { id: "1", name: "Buck", members: ["1"] }
        ]
      }
    }
  }
];

it("renders without crashing", () => {
  const ContextMock = jest.fn(
    () => createContext({ jwt: "", user: guest }));

  const Context = new ContextMock();

  const NaviMock =
  jest.fn<
  NavigationScreenProp<NavigationRoute<NavigationParams>,
  NavigationParams>>(() => ({
    navigate: jest.fn()
  }));

  const navigation = new NaviMock();

  const rendered = renderer.create(
    <MockedProvider mocks={mocks} addTypename={false}>
      <Context.Provider value={{ jwt: "", user: guest }}>
        <Feed
          navigation={navigation}
          detailsPage={"FeedEntries"}
          query={FeedEntries}
        />
      </Context.Provider>
    </MockedProvider>
  ).toJSON();

  expect(rendered).toBeTruthy();
});
