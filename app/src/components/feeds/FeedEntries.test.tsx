import React, { createContext } from "react";
import {
  NavigationParams,
  NavigationRoute,
  NavigationScreenProp
} from "react-navigation";
import renderer from "react-test-renderer";

import { Availability, Event, EventType, User } from "ourlineslib";
import { guest } from "../contexts/LoginContext";
import { FeedEntry } from "./FeedEntries";

it("renders without crashing", () => {
  const members = [
    new User({
      availability: Availability.AVAILABLE,
      email: "email@example.com",
      id: "1"
    })];
  const event = new Event({
    eventType: EventType.Text,
    id: "1",
    ispublic: false,
    members
  });

  const ContextMock = jest.fn(
    () => createContext({ jwt: "", user: guest }));

  const Context = new ContextMock();

  const NaviMock = jest.fn<
  NavigationScreenProp<NavigationRoute<NavigationParams>,
  NavigationParams>>(() => ({
      navigate: jest.fn()
  }));

  const navigation = new NaviMock();

  const rendered = renderer.create(
    <Context.Provider value={{ jwt: "", user: guest }}>
      <FeedEntry
        item={event}
        navigation={navigation}
        detailsPage={""}
        addSelected={() => jest.fn()}
        removeSelected={() => jest.fn()}
      />
    </Context.Provider>
  ).toJSON();

  expect(rendered).toBeTruthy();
});
