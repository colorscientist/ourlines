import { DocumentNode } from "graphql";
import gql from "graphql-tag";
import React, { Component } from "react";
import { NavigationScreenProp } from "react-navigation";

import { Event } from "ourlineslib";
import { LoginConsumer, LoginState } from "../contexts/LoginContext";
import { Feed } from "./Feed";

export const FeedEntries: DocumentNode = gql`
  query FeedEntries(
      $filters: [String]!, $cursor: String, $jwt: String) {
    events(
        input: {
            filters: $filters,
            cursor: $cursor,
            jwt: $jwt }) {
      events {
          id
          name
          members
      }
      cursor
    }
  }
`;

interface EventsFeedProps {
    navigation: NavigationScreenProp<{}>;
    filters: string[];
}

export class EventsFeed extends Component<EventsFeedProps> {
    constructor(props: EventsFeedProps) {
        super(props);
    }

    public render() {

        return (
            <LoginConsumer>
            {this._MakeEventsFeed}
            </LoginConsumer>
        );
    }

    private _MakeEventsFeed = (result: LoginState) => {
        const { navigation, filters } = this.props;

        const { jwt } = result;

        return (
        <Feed<Event>
            query={FeedEntries}
            navigation={navigation}
            detailsPage={"Event"}
            variables={{ filters, jwt }}
            filters={filters}
        />
        );
    }
}
