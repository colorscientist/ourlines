import { DocumentNode } from "graphql";

import gql from "graphql-tag";

import { NavigationScreenProp } from "react-navigation";

import { FeedEntryItem, User } from "ourlineslib";
import React, { Component } from "react";

import { LoginConsumer } from "../contexts/LoginContext";
import { Feed } from "./Feed";

export const UserFriends: DocumentNode = gql`
    query UserFriends($ids: [ID!]!) {
        users(input: { ids: $ids }) {
            id
            email
            name
            friends
            picture {
                id
            }
            availability
        }
    }
`;

interface UserFeedProps {
    navigation: NavigationScreenProp<{}>;
    selectedUsers: User[];
    addSelectedUser: (user: User) => void;
    removeSelectedUser: (user: User) => void;
    appendedItems: FeedEntryItem[];
}

const MakeUserFeed = (
  { user, props }:
  { user: User, props: UserFeedProps }) => {

  if (!user.friends) {
    return;
  }

  return (
      <Feed<User>
          navigation={props.navigation}
          detailsPage={"CreateEvent"}
          query={UserFriends}
          variables={{ ids: user.friends }}
          multiselect={true}
          selected={props.selectedUsers}
          addSelected={props.addSelectedUser}
          removeSelected={props.removeSelectedUser}
          appendedItems={props.appendedItems}
      />
  );
};

export class UserFeed extends Component<UserFeedProps> {
    constructor(props: UserFeedProps) {
        super(props);
    }

    public render() {
        return (
            <LoginConsumer>
                {({user}) => MakeUserFeed({ user, props: this.props })}
            </LoginConsumer>
        );
    }
}
