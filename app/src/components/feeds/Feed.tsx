import { DocumentNode } from "graphql";
import { Event, FeedEntryItem, User  } from "ourlineslib";
import React, { Component } from "react";
import { Query, QueryResult } from "react-apollo";
import {
    ActivityIndicator,
    FlatList,
    InteractionManager,
    Keyboard,
    StyleProp,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    View,
    ViewStyle
} from "react-native";
import { NavigationScreenProp } from "react-navigation";

import { ItemSeparator } from "../Separator";
import { FeedEntry } from "./FeedEntries";

interface Props<T extends FeedEntryItem> {
    filters?: string[];
    detailsPage: string;
    navigation: NavigationScreenProp<{}>;
    query: DocumentNode;
    variables?: any;
    multiselect?: boolean;
    selected?: T[];
    addSelected?: (item: T) => void;
    removeSelected?: (item: T) => void;
    appendedItems?: any[];
    returnItems?: (items: T[]) => void;
    itemsFromSearch?: any[];
}

interface State {
    animationsFinished: boolean;
    refreshing: boolean;
    itemsFromSearch?: FeedEntryItem[];
}

const updateQuery = (previousResult: any, fetchMoreResult: any) => {
    if (!fetchMoreResult) { return previousResult; }
    const previousEntry = previousResult.entry;
    const newFeedEntries = fetchMoreResult.moreEntries.entries;
    const newCursor = fetchMoreResult.moreComments.cursor;

    return {
        // By returning `cursor` here,
        // we update the `fetchMore` function
        // to the new cursor.
        cursor: newCursor,
        entry: {
            // Put the new comments in the front of the list
            comments: [...newFeedEntries, ...previousEntry.entries]
        }
    };
};

const renderItem =
<T extends FeedEntryItem>(item: T, props: Props<T>) => {
    const selectList = props.selected &&
    props.selected.length > 0 ?
    props.selected : undefined;

    return (
        <FeedEntry {...props} item={item} selected={selectList}/>
    );
};

interface Data {
    events?: {
        events: Event[];
        cursor?: string;
    };
    users?: User[];
    cursor?: string;
}

interface QueryFeedResultProps {
    result: QueryResult<Data>;
    refreshing: boolean;
    setRefreshing: () => void;
}

const QueryFeedResult = <T extends FeedEntryItem>(
    props: QueryFeedResultProps & Props<T>) => {

    const {
        data,
        fetchMore,
        refetch
    } = props.result;

    const {
        query,
        variables,
        setRefreshing,
        appendedItems,
        returnItems,
        itemsFromSearch
     } = props;

    if (!data) {
        return (<Text>Loading...</Text>);
    }

    const list: any[] = data.users ||
    data.events && data.events.events ||
    [];

    const listStyle: ViewStyle = {
        flex: 1
    };

    const onEndReached = () => {
        if ("cursor" in data) {
            fetchMore({ query, variables, updateQuery });
        }
    };

    const onRefresh = () => {
        setRefreshing();
        refetch();
    };

    const ITEM_HEIGHT = 76;

    const itemLayoutFn = (_: T[] | null, index: number) => (
        {length: ITEM_HEIGHT, offset: ITEM_HEIGHT * index, index}
      );

    const listData = itemsFromSearch ||
    (appendedItems ? [...list, ...appendedItems] : list);

    const navToSearch = () => props.navigation.navigate(
        "SearchModal",
        {
            data: (appendedItems ? [...list, ...appendedItems] : list),
            returnItems
        });

    const listHeader = (
    <TouchableWithoutFeedback
        onPress={Keyboard.dismiss}
        accessible={false}
    >
        <View style={{flex: 1}}>
            <TextInput
                onFocus={navToSearch}
                placeholder={"Search"}
            />
        </View>
    </TouchableWithoutFeedback>);

    const filteredData = listData.filter((x) => {
        return x.availability && x.availability !== "HIDDEN";
    });

    const content = (
    <FlatList
        data={filteredData}
        renderItem={({item}) => renderItem(item, props)}
        keyExtractor={(item) => item.id}
        onEndReached={onEndReached}
        onRefresh={onRefresh}
        refreshing={props.refreshing}
        style={listStyle}
        ItemSeparatorComponent={() => <ItemSeparator />}
        contentContainerStyle={{ flex: 1 }}
        getItemLayout={itemLayoutFn}
        initialNumToRender={8}
        maxToRenderPerBatch={2}
        onEndReachedThreshold={0.5}
        ListHeaderComponent={listHeader}
    />
    );

    const viewStyle: StyleProp<ViewStyle> = {
        alignItems: "center",
        flex: 1,
        flexDirection: "row",
        justifyContent: "center"
    };

    return (
        <View
            style={viewStyle}
        >
            {content}
        </View>
    );
};

export class Feed<T extends FeedEntryItem>
extends Component<Props<T>, State> {
    constructor(props: Props<T>) {
        super(props);

        this.state = {
            animationsFinished: false,
            refreshing: false
        };
    }

    public componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            this.setState({ animationsFinished: true });
        });
    }
    public setRefreshing = () => this.setState({ refreshing: true });

    public render() {
        const {
            query,
            variables
        } = this.props;

        if (!this.state.animationsFinished) {
            return (<ActivityIndicator />);
        }

        const returnItems = (items: FeedEntryItem[]) => {
            this.setState({ itemsFromSearch: items });
        };

        const queryResult = (data: QueryResult<Data>) => (
        <QueryFeedResult<T>
            result={data}
            {...this.props}
            refreshing={this.state.refreshing}
            setRefreshing={this.setRefreshing}
            returnItems={returnItems}
            itemsFromSearch={this.state.itemsFromSearch}
        />
        );

        return (
            <Query
                query={query}
                variables={variables}
                pollInterval={500}
                onCompleted={() => this.setState({ refreshing: false })}
                fetchPolicy="cache-and-network"
            >
            {queryResult}
            </Query>);
    }
}
