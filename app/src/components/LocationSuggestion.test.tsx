import React from "react";
import renderer from "react-test-renderer";

import { LocationSuggestor } from "./LocationSuggestion";

it("Renders without crashing", () => {
  const rendered = renderer.create(<LocationSuggestor />).toJSON();

  expect(rendered).toBeTruthy();
});
