import { InMemoryCache } from "apollo-cache-inmemory";
import { ApolloClient } from "apollo-client";
import { HttpLink } from "apollo-link-http";
import fetch from "isomorphic-unfetch";
import React from "react";
import { ApolloProvider } from "react-apollo";
import { StatusBar } from "react-native";

import { LoginProvider } from "./components/contexts/LoginContext";
import { AppNav } from "./components/Navigators";

export const link = new HttpLink({
  fetch,
  uri: "http://192.168.1.9:4050/graphql"
});

export const client = new ApolloClient({
  cache: new InMemoryCache(),
  link
});

export class App extends React.Component {
  public render() {
    return (
      <ApolloProvider client={client}>
        <LoginProvider>
          <StatusBar
            backgroundColor="#003f00"
            barStyle="default"
            hidden={false}
          />
          <AppNav />
        </LoginProvider>
      </ApolloProvider>
    );
  }
}
