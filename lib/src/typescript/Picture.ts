// tslint:disable-next-line:no-any
export const isPicture = (arg: any): arg is Picture => {
    return arg.uri !== "undefined" && arg.local !== "undefined";
};

export class Picture {
    public id: string;
    public uri: string;
    public local: boolean;

    constructor(id: string, uri: string, local: boolean) {
        this.id = id;
        this.uri = uri;
        this.local = local;
    }
}
