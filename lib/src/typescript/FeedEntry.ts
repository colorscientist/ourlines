import { Location } from "./Location";
import { Picture } from "./Picture";
import { Availability } from "./User";

export interface FeedEntryItem {
  id: string;
  name: string;
  email?: string;
  picture?: Picture;
  location?: Location;
  availability?: Availability;
}
