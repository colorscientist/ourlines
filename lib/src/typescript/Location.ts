export interface BaseLocationInfo {
    name?: string;
}

export interface CoordinateLocationInfo extends BaseLocationInfo {
    name?: string;
    coordinates: {
        lat: number;
        lon: number;
    };
}

export interface AddressLocationInfo extends BaseLocationInfo {
    name?: string;
    address: string;
}

export interface NameLocationInfo extends BaseLocationInfo {
    name: string;
}

export type LocationInfo =
    CoordinateLocationInfo
    | AddressLocationInfo
    | NameLocationInfo;

export class Location {
    public name: string;
    public coordinates?: {
        lat: number;
        lon: number;
    };
    public address?: string;

    constructor(locationInfo: LocationInfo) {
        this.coordinates =
        (locationInfo as CoordinateLocationInfo).coordinates ||
        { lat: 0, lon: 0 };

        this.address =
        ((locationInfo as AddressLocationInfo).address) ||
        undefined;

        const { name } = locationInfo;
        this.name = name ||
        this.address ||
        `(${this.coordinates.lat}, ${this.coordinates.lon})`;
    }
}
