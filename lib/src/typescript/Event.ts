import { FeedEntryItem } from "./FeedEntry";
import { Location } from "./Location";
import { Picture } from "./Picture";
import { User } from "./User";

export enum EventType {
    Text = "Text",
    Call = "Call",
    PersonalMeetup = "Personal Meetup",
    Party = "Party"
}

// Disable no-any because the typeguard helps us introduce strictness
// tslint:disable-next-line:no-any
export const isEvent = (arg: any): arg is Event => {
    return arg.eventType !== "undefined";
};

export const makeEventName = (
    users: User[],
    eventType: EventType): string => {
    const eventGoers = users.map((user) => user.name);

    const titleEventGoers = eventGoers.length === 1 ?
    eventGoers[0] : eventGoers.length <= 3 ?
    eventGoers.slice(0, 2)
    .join(", ").concat(`, and ${eventGoers[2]}`) :
    eventGoers.slice(0, 2)
    .join(", ").concat(", and more");

    const titleEventType: string = eventType;

    return `${titleEventType} with ${titleEventGoers}`;
};

export class Event implements FeedEntryItem {
    public id: string;
    public members: User[];
    public invited: User[];
    public name: string;
    public eventType: EventType;
    public picture?: Picture;
    public location?: Location;
    public time?: string;
    public ispublic: boolean;

    constructor(args: {
        id: string,
        members: User[],
        eventType: EventType,
        picture?: Picture,
        name?: string,
        time?: string,
        ispublic: boolean}) {
        this.id = args.id;
        this.members = args.members;
        this.picture = args.picture;
        this.eventType = args.eventType;
        this.name = args.name ||
        makeEventName(this.members, this.eventType);
        this.time = args.time;
        this.ispublic = false;
        this.invited = [];
    }
}
