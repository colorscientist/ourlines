import { FeedEntryItem } from "./FeedEntry";
import { Picture } from "./Picture";

// tslint:disable-next-line:no-any
export const isUser = (arg: any): arg is User => {
    return arg.email !== "undefined";
};

export enum Availability {
    AVAILABLE = "AVAILABLE",
    IN_PUBLIC_EVENT = "IN_PUBLIC_EVENT",
    HIDDEN = "HIDDEN"
}

export class User implements FeedEntryItem {
    public id: string;
    public email: string;
    public name: string;
    public availability: Availability;
    public phone?: string;
    public friends?: string[];
    public muted?: string[];
    public interests?: string[];
    public picture?: Picture;

    constructor(args: {
        id: string,
        email: string,
        availability: Availability,
        phone?: string,
        name?: string,
        picture?: Picture,
        friends?: string[],
        muted?: string[],
        interests?: string[] }) {

        this.id = args.id;
        this.email = args.email;
        this.name = args.name || args.email;
        this.picture = args.picture;
        this.phone = args.phone;
        this.friends = args.friends || [];
        this.muted = args.muted || [];
        this.interests = args.interests || [];
        this.availability = args.availability;
    }
}
