import { Event, EventType } from "./Event";
import { Availability, User } from "./User";

it("Initializes with all arguments", () => {
    const user = new User(
        {
            availability: Availability.AVAILABLE,
            email: "email@example.com",
            id: "1",
            name: "Alice"
        });
    const event = new Event(
        {
            eventType: EventType.Text,
            id: "",
            ispublic: false,
            members: [user],
            name: "Party!!1!one"
        });

    expect(event).toBeTruthy();
});

it("Infers event name when not explicitly set", () => {
    const user = new User(
        {
            availability: Availability.AVAILABLE,
            email: "email@example.com",
            id: "1",
            name: "Alice"
        });
    const event = new Event(
        {
            eventType: EventType.Text,
            id: "",
            ispublic: false,
            members: [user]
        });

    expect(event.name).toBe("Text with Alice");
});

it("Makes event name with three users", () => {
    const users = ["Alice", "Bob", "Charlie"].map(
        (name, idx) => new User(
            {
                availability: Availability.AVAILABLE,
                email: "email@example.com",
                id: idx.toString(),
                name
            }));

    const event = new Event(
        {
            eventType: EventType.Text,
            id: "",
            ispublic: false,
            members: users
        });

    expect(event.name).toBe("Text with Alice, Bob, and Charlie");
});

it(`Makes event name no longer than three names,
 cutting off at two and adding "and more"`, () => {
    const users = ["Alice", "Bob", "Charlie", "David"].map(
        (name, idx) => new User(
            {
                availability: Availability.AVAILABLE,
                email: "email@example.com",
                id: idx.toString(),
                name
            }));

    const event = new Event(
        {
            eventType: EventType.Text,
            id: "",
            ispublic: false,
            members: users
        });

    expect(event.name).toBe("Text with Alice, Bob, and more");
});
