import { Picture } from "./Picture";
import { Availability, User } from "./User";

it("Initializes with all arguments", () => {
    const user = new User(
        {
            availability: Availability.AVAILABLE,
            email: "email@example.com",
            id: "1",
            name: "Alice",
            phone: "555-555-5555",
            picture: new Picture("1", "http://example.com/img", false)
        });
    expect(user).toBeTruthy();
});
