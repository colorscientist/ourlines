import { Location } from "./Location";

it("Instantiates with name", () => {
    const loc = new Location({ name: "Home" });
    expect(loc).toBeTruthy();
});

it("Instantiates with address", () => {
    const loc = new Location({ address: "1 1st Street, Austin, Texas"});
    expect(loc.name).toBeTruthy();
    expect(loc.address).toBeTruthy();
});

it(`Instantiates with name and address,
and does not use address as name`, () => {
    const loc = new Location(
        {
            address: "1 1st Street, Austin, Texas",
            name: "Building"
        });

    expect(loc.name).toBeTruthy();
    expect(loc.address).toBeTruthy();
    expect(loc.name).not.toBe(loc.address);
});

it("Instantiates with coordinates", () => {
    const loc = new Location({ coordinates: { lat: 1, lon: 1 }});

    expect(loc.name).toBeTruthy();
    expect(loc.address).toBeTruthy();
});

it(`Instantiates with name and coordinates,
and does not use coordinates as name`, () => {
    const loc = new Location(
        {
            coordinates: { lat: 1, lon: 1 },
            name: "Place"
        });

    expect(loc.name).toBeTruthy();
    expect(loc.address).toBeTruthy();
    expect(loc.name).not.toBe(loc.address);
});
