import { Client } from "cassandra-driver";
// import { Logger } from "pino";
import { S3 } from "../../node_modules/aws-sdk/index";

export interface Context {
  client: Client;
  logger: any;
  s3: S3;
}
