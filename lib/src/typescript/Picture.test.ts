import { Picture } from "./Picture";

it("Instantiates with all arguments", () => {
    const pic = new Picture("none", "none", true);

    expect(pic).toBeTruthy();
});
