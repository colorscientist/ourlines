# Contribution Guide

1. All changes must be made in branches from master
2. Branches should be merged as rapidly as possible to avoid merge conflicts (i.e., on the order of several times each day)
3. No pull requests without unit tests
4. Typescript must pass lint as defined in tslint.json
5. Branches should have a branch folder, categorizing them into one of the branch types

Branch Label | Description
--- | ---
feat | This is for features, changes which add some new use or value, including changes to styling
fix | This is for fixes, which remove bugs or unintended outcomes
