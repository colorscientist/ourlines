import Fonts from '../Fonts';
import { PureComponent } from 'react';
import { Formik, Form, Field } from 'formik';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';

const addEmailToEmailBlast = gql`
  mutation AddEmailToEmailBlast($email: String!, $blastname: String!) {
    addEmailToBlast(input: { email: $email, blastname: $blastname })
  }
`;
export default class Index extends PureComponent {
  state = {
    email: ""
  }

  public componentDidMount() {
    Fonts();
  }

  public render() {
    return (
      <div className="grid">
      <div className="navbar">
      </div>
      <h1 className="title">ourlines</h1>
      <div className="store-btns">
      </div>
      <Mutation mutation={addEmailToEmailBlast}>
        {(mutation) => {
          return (
            <Formik
            initialValues={{ email: "" }}
            onSubmit={(values, actions) => {
              setTimeout(() => {
                mutation({ variables: { email: values.email, blastname: "Initial Release" }})
                actions.setSubmitting(false);
              }, 1000);
            }}
            render={({ errors, touched, isSubmitting }) => (
              <Form>
                <Field
                  type="email"
                  name="email"
                  placeholder="email@example.com"
                />
                {errors.email && touched.email && <div>{errors.email}</div>}
                <button type="submit" disabled={isSubmitting}>
                  Submit
                </button>
              </Form>
            )}
          />
          );
        }}
      </Mutation>
      <style jsx>{`
      .login-link {
        grid-area: login;
        justify-self: center;
        align-self: center;
        padding-right: 50px;
      }

      .navbar {
        grid-area: nav;
        display: grid;
        grid-template-columns: auto 100px;
        grid-template-areas: ". login";
      }

      .title {
        align-self: center;
        justify-self: center;
        grid-area: title;
        font-size: 150px;
        font-family: Pacifico;
      }

      .grid {
        display: grid;
        grid-gap: 30px;
        grid-template-columns: repeat(3, 1fr);
        grid-template-rows: 75px 150px auto;
        grid-template-areas:
        "nav   nav   nav  "
        ".     title .    "
        ".     email .    ";
      }

      .email-input {
        display: grid;
        grid-area: email;
      }

      /* 
        ##Device = Desktops
        ##Screen = 1281px to higher resolution desktops
      */
      @media screen and (min-width: 1281px) {

        //CSS

      }

      /* 
        ##Device = Laptops, Desktops
        ##Screen = B/w 1025px to 1280px
      */
      @media screen and (min-width: 1025px) and (max-width: 1280px) {

        //CSS

      }

      /* 
        ##Device = Tablets, Ipads (portrait)
        ##Screen = B/w 768px to 1024px
      */
      @media screen and (min-width: 768px) and (max-width: 1024px) {
        
        //CSS
        
      }
  
      /* 
        ##Device = Tablets, Ipads (landscape)
        ##Screen = B/w 768px to 1024px
      */
      @media screen and (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {
        
        //CSS
        
      }
  
      /* 
        ##Device = Low Resolution Tablets, Mobiles (Landscape)
        ##Screen = B/w 481px to 767px
      */
      @media screen and (min-width: 481px) and (max-width: 767px) {
        
        //CSS
        
      }
  
      /* 
        ##Device = Most of the Smartphones Mobiles (Portrait)
        ##Screen = B/w 320px to 479px
      */
      @media screen and (min-width: 320px) and (max-width: 480px) {
        
        //CSS
        
      }
      `}
      </style>
    </div>
    );
  }
}

