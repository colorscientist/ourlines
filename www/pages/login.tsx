import { PureComponent } from "react";
import { LoginConsumer } from "../components/LoginContext";

export default class LoginPage extends PureComponent {
  public state = {
    email: "",
    password: ""
  }

  public render = () => (
  <div>
    <LoginConsumer>
      {(login) => {
        const { email, password } = this.state;

        console.log(email, password, login.jwt);

        if (login.logIn) {
          return (
            <div className="grid">
              <input
                type="email"
                placeholder="email@example.com"
                value={this.state.email}
                onChange={(event) => this.setState({email: event.target.value})}
              />
              <input
                type="password"
                placeholder="password"
                value={this.state.password}
                onChange={(event) => this.setState({password: event.target.value})}
              />
              <button title="Login" onClick={() => login.logIn({ variables: { email, password }})}/>
              <p>{login.jwt}</p>
              <style>{`
              .grid {
                display: grid;
                grid-template-columns: repeat(7, 1fr);
                grid-template-rows: 75px repeat(2, 1fr);        
              }
              `}
              </style>
            </div>
          )
        }
      }}
    </LoginConsumer>
  </div>
  );
}