import React from 'react'
import App, { Container } from 'next/app'
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { ApolloLink } from 'apollo-link';
import fetch from "isomorphic-unfetch";
import { ApolloProvider } from "react-apollo";
import { LoginProvider } from '../components/LoginContext';

const client = new ApolloClient({
  link: ApolloLink.from([
    onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors)
        graphQLErrors.map(({ message, locations, path }) =>
          console.log(
            `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
          ),
        );
      if (networkError) console.log(`[Network error]: ${networkError}`);
    }),
    new HttpLink({
      uri: 'http://192.168.1.9:4050/graphql',
      credentials: 'same-origin',
      fetch,
      fetchOptions: {
        credentials: "same-origin",
        mode: "cors"
      }
    })
  ]),
  cache: new InMemoryCache()
});

export default class OurlinesApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {}

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    return { pageProps }
  }

  render () {
    const { Component, pageProps } = this.props

    return (
      <Container>
        <ApolloProvider client={client}>
          <LoginProvider>
            <Component {...pageProps} />
          </LoginProvider>
        </ApolloProvider>
      </Container>
    )
  }
}
