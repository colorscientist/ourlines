import FontFaceObserver from 'fontfaceobserver';

const Fonts = () => {
  const link = document.createElement('link')
  link.href = 'https://fonts.googleapis.com/css?family=Pacifico:300,400,500,700,900'
  link.rel = 'stylesheet'

  document.head.appendChild(link)

  const roboto = new FontFaceObserver('Pacifico')

  roboto.load().then(() => {
    document.documentElement.classList.add('pacifico')
  })
}

export default Fonts